$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/rkeough/git/babson-automation/Include/features/Forms/Form453/Graduate152.feature");
formatter.feature({
  "name": "admission/graduate-school/",
  "description": "  I want to submit and verify form submits with the correct data",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Form"
    },
    {
      "name": "@152"
    },
    {
      "name": "@Graduate"
    }
  ]
});
formatter.scenario({
  "name": "Submit Certificate Management Form",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Form"
    },
    {
      "name": "@152"
    },
    {
      "name": "@Graduate"
    },
    {
      "name": "@Grad"
    },
    {
      "name": "@152"
    },
    {
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "name": "I am on the homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "Graduate152.Homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I am on the form form page",
  "keyword": "When "
});
formatter.match({
  "location": "Graduate152.FormPage()"
});
formatter.result({
  "error_message": "groovy.lang.MissingPropertyException: No such property: form for class: forms.Graduate152\r\n\tat org.codehaus.groovy.runtime.ScriptBytecodeAdapter.unwrap(ScriptBytecodeAdapter.java:66)\r\n\tat org.codehaus.groovy.runtime.callsite.GetEffectivePogoPropertySite.getProperty(GetEffectivePogoPropertySite.java:87)\r\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.callGroovyObjectGetProperty(AbstractCallSite.java:310)\r\n\tat forms.Graduate152.FormPage(Graduate152.groovy:57)\r\n\tat ✽.I am on the form form page(C:/Users/rkeough/git/babson-automation/Include/features/Forms/Form453/Graduate152.feature:8)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "I input the first and last name on the form",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.FirstLastName()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input an email address",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputEmail()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a program",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectAProgramOfInterest()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a term",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectATermOfEnrollment()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input a phone number",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputPhoneNumber()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a country",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectACountry()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input a state",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputState()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input a city",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputCity()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input the postal code",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputPostalZode()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I click the submit button",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.clickSubmit()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I am on the correct thankyou page",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.thankYouPage()"
});
formatter.result({
  "status": "skipped"
});
});