$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/rkeough/git/babson-automation/Include/features/Forms/152Graduate/Graduate152.feature");
formatter.feature({
  "name": "admission/graduate-school/certificate-in-advanced-management/",
  "description": "  I want to submit and verify form submits with the correct data",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Form"
    },
    {
      "name": "@152"
    },
    {
      "name": "@Graduate"
    }
  ]
});
formatter.scenarioOutline({
  "name": "Submit Certificate Management Form",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Grad"
    },
    {
      "name": "@152"
    },
    {
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "name": "I am on the homepage",
  "keyword": "Given "
});
formatter.step({
  "name": "I am on the form \"\u003cform\u003e\" page",
  "keyword": "When "
});
formatter.step({
  "name": "I input the first and last name on the form",
  "keyword": "And "
});
formatter.step({
  "name": "I input an email address",
  "keyword": "And "
});
formatter.step({
  "name": "I select a program \"\u003cprogram\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "I select a term \"\u003cterm\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "I input a phone number",
  "keyword": "And "
});
formatter.step({
  "name": "I select a country",
  "keyword": "And "
});
formatter.step({
  "name": "I input a state",
  "keyword": "And "
});
formatter.step({
  "name": "I input a city",
  "keyword": "And "
});
formatter.step({
  "name": "I input the postal code",
  "keyword": "And "
});
formatter.step({
  "name": "I click the submit button",
  "keyword": "And "
});
formatter.step({
  "name": "I am on the correct \"\u003cthankYou\u003e\" page",
  "keyword": "And "
});
formatter.step({
  "name": "I get the submit request",
  "keyword": "And "
});
formatter.step({
  "name": "I delete the form record",
  "keyword": "Then "
});
formatter.step({
  "name": "I get the submit request",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "form",
        "program",
        "term",
        "thankYou"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/academics/graduate-school/",
        "tfa_1184",
        "tfa_1186",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/academics/graduate-school/mba/full-time-mba/",
        "tfa_1184",
        "tfa_1257",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/business-programs-wes/master-of-science-in-management-in-entrepreneurial-leadership/",
        "tfa_1043",
        "tfa_1188",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/academics/graduate-school/mba/",
        "tfa_1043",
        "tfa_1223",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/academics/graduate-school/mba/mba-miami/",
        "tfa_1208",
        "tfa_1226",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/academics/graduate-school/ms-business-analytics/",
        "tfa_1208",
        "tfa_1269",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/academics/graduate-school/ms-in-entrepreneurial-leadership/",
        "tfa_1122",
        "tfa_1277",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/academics/graduate-school/mba/part-time-mba/",
        "tfa_1063",
        "tfa_1281",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/admission/graduate-school/request-information/",
        "tfa_1075",
        "tfa_1251",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/graduate/academics/part-time-business-programs/certificate-in-advanced-management/",
        "tfa_1156",
        "tfa_1246",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Submit Certificate Management Form",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Form"
    },
    {
      "name": "@152"
    },
    {
      "name": "@Graduate"
    },
    {
      "name": "@Grad"
    },
    {
      "name": "@152"
    },
    {
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "name": "I am on the homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "Graduate152.Homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I am on the form \"https://www.babson.edu/academics/graduate-school/\" page",
  "keyword": "When "
});
formatter.match({
  "location": "Graduate152.FormPage(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I input the first and last name on the form",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.FirstLastName()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I input an email address",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputEmail()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I select a program \"tfa_1184\"",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectAProgramOfInterest(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I select a term \"tfa_1186\"",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectATermOfEnrollment(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I input a phone number",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputPhoneNumber()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I select a country",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectACountry()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I input a state",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputState()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I input a city",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputCity()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I input the postal code",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputPostalZode()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click the submit button",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.clickSubmit()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I am on the correct \"https://www.babson.edu/graduate/admissions/thank-you/\" page",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.thankYouPage(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I get the submit request",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.submitRequest()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I delete the form record",
  "keyword": "Then "
});
formatter.match({
  "location": "Graduate152.deleteRequest()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I get the submit request",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.submitRequest()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Submit Certificate Management Form",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Form"
    },
    {
      "name": "@152"
    },
    {
      "name": "@Graduate"
    },
    {
      "name": "@Grad"
    },
    {
      "name": "@152"
    },
    {
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "name": "I am on the homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "Graduate152.Homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I am on the form \"https://www.babson.edu/academics/graduate-school/mba/full-time-mba/\" page",
  "keyword": "When "
});
formatter.match({
  "location": "Graduate152.FormPage(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I input the first and last name on the form",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.FirstLastName()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I input an email address",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputEmail()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I select a program \"tfa_1184\"",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectAProgramOfInterest(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I select a term \"tfa_1257\"",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectATermOfEnrollment(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I input a phone number",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputPhoneNumber()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I select a country",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectACountry()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I input a state",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputState()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I input a city",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputCity()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I input the postal code",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputPostalZode()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click the submit button",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.clickSubmit()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I am on the correct \"https://www.babson.edu/graduate/admissions/thank-you/\" page",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.thankYouPage(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I get the submit request",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.submitRequest()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I delete the form record",
  "keyword": "Then "
});
formatter.match({
  "location": "Graduate152.deleteRequest()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I get the submit request",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.submitRequest()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Submit Certificate Management Form",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Form"
    },
    {
      "name": "@152"
    },
    {
      "name": "@Graduate"
    },
    {
      "name": "@Grad"
    },
    {
      "name": "@152"
    },
    {
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "name": "I am on the homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "Graduate152.Homepage()"
});
