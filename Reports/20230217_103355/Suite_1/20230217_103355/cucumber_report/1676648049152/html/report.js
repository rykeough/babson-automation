$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/rkeough/git/babson-automation/Include/features/Forms/Form321/Form321.feature");
formatter.feature({
  "name": "Form 321",
  "description": "  I want to be able to fill out fields of the forms and submit",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@445"
    }
  ]
});
formatter.scenarioOutline({
  "name": "Submit program MBA form",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "name": "I am on the homepage",
  "keyword": "Given "
});
formatter.step({
  "name": "I am on the form \"\u003cform\u003e\" page",
  "keyword": "When "
});
formatter.step({
  "name": "I input the first and last name on the form",
  "keyword": "And "
});
formatter.step({
  "name": "I input an email address",
  "keyword": "And "
});
formatter.step({
  "name": "I select a program \"\u003cprogram\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "I select a term \"\u003cterm\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "I input a phone number",
  "keyword": "And "
});
formatter.step({
  "name": "I select a country",
  "keyword": "And "
});
formatter.step({
  "name": "I am on the correct \"\u003cthankYou\u003e\" page",
  "keyword": "And "
});
formatter.step({
  "name": "I get the submit request",
  "keyword": "And "
});
formatter.step({
  "name": "I delete the form record",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "form",
        "program",
        "term",
        "thankYou"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1037",
        "tfa_1093",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1037",
        "tfa_1110",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1037",
        "tfa_1368",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1037",
        "tfa_1369",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1037",
        "tfa_1399",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1037",
        "tfa_1403",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1037",
        "tfa_1426",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1037",
        "tfa_1427",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1043",
        "tfa_1099",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1043",
        "tfa_1100",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1043",
        "tfa_1101",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1043",
        "tfa_1111",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1043",
        "tfa_1370",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1043",
        "tfa_1371",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1043",
        "tfa_1372",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1043",
        "tfa_1395",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1043",
        "tfa_1400",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1043",
        "tfa_1412",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1116",
        "tfa_1375",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1116",
        "tfa_1376",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1116",
        "tfa_1377",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1116",
        "tfa_1378",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1116",
        "tfa_1379",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1116",
        "tfa_1380",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1116",
        "tfa_1381",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1116",
        "tfa_1396",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1116",
        "tfa_1401",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1116",
        "tfa_1413",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1116",
        "tfa_1428",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1057",
        "tfa_1112",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1057",
        "tfa_1382",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1057",
        "tfa_1404",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1057",
        "tfa_1431",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1063",
        "tfa_1113",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1063",
        "tfa_1383",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1063",
        "tfa_1405",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1063",
        "tfa_1425",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1069",
        "tfa_1114",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1069",
        "tfa_1384",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1069",
        "tfa_1406",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1069",
        "tfa_1432",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1075",
        "tfa_1107",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1075",
        "tfa_1108",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1075",
        "tfa_1109",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1075",
        "tfa_1115",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1075",
        "tfa_1392",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1075",
        "tfa_1393",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1075",
        "tfa_1394",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1075",
        "tfa_1398",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1075",
        "tfa_1402",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1075",
        "tfa_1414",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1407",
        "tfa_1410",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1407",
        "tfa_1411",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1407",
        "tfa_1429",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    },
    {
      "cells": [
        "https://www.babson.edu/grad/emss/part-time-graduate-programs/",
        "tfa_1407",
        "tfa_1430",
        "https://www.babson.edu/graduate/admissions/thank-you/"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Submit program MBA form",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@445"
    },
    {
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "name": "I am on the homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "Graduate152.Homepage()"
});
formatter.result({
  "error_message": "com.kms.katalon.core.webui.exception.BrowserNotOpenedException: Browser is not opened\nBuild info: version: \u00273.141.59\u0027, revision: \u0027e82be7d358\u0027, time: \u00272018-11-14T08:25:53\u0027\nSystem info: host: \u0027L18-0032\u0027, ip: \u002710.0.0.226\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_181\u0027\nDriver info: driver.version: DriverFactory$getWebDriver\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.verifyWebDriverIsOpen(DriverFactory.java:985)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.verifyWebDriver(DriverFactory.java:970)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.getWebDriver(DriverFactory.java:956)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory$getWebDriver.call(Unknown Source)\r\n\tat org.codehaus.groovy.runtime.callsite.CallSiteArray.defaultCall(CallSiteArray.java:47)\r\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:116)\r\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:120)\r\n\tat forms.Graduate152.Homepage(Graduate152.groovy:49)\r\n\tat ✽.I am on the homepage(C:/Users/rkeough/git/babson-automation/Include/features/Forms/Form321/Form321.feature:7)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "I am on the form \"https://www.babson.edu/grad/emss/part-time-graduate-programs/\" page",
  "keyword": "When "
});
formatter.match({
  "location": "Graduate152.FormPage(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input the first and last name on the form",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.FirstLastName()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input an email address",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputEmail()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a program \"tfa_1037\"",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectAProgramOfInterest(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a term \"tfa_1093\"",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectATermOfEnrollment(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input a phone number",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputPhoneNumber()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a country",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectACountry()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I am on the correct \"https://www.babson.edu/graduate/admissions/thank-you/\" page",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.thankYouPage(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I get the submit request",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.submitRequest()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I delete the form record",
  "keyword": "Then "
});
formatter.match({
  "location": "Graduate152.deleteRequest()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "name": "Submit program MBA form",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@445"
    },
    {
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "name": "I am on the homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "Graduate152.Homepage()"
});
formatter.result({
  "error_message": "com.kms.katalon.core.webui.exception.BrowserNotOpenedException: Browser is not opened\nBuild info: version: \u00273.141.59\u0027, revision: \u0027e82be7d358\u0027, time: \u00272018-11-14T08:25:53\u0027\nSystem info: host: \u0027L18-0032\u0027, ip: \u002710.0.0.226\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_181\u0027\nDriver info: driver.version: DriverFactory$getWebDriver\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.verifyWebDriverIsOpen(DriverFactory.java:985)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.verifyWebDriver(DriverFactory.java:970)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.getWebDriver(DriverFactory.java:956)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory$getWebDriver.call(Unknown Source)\r\n\tat forms.Graduate152.Homepage(Graduate152.groovy:49)\r\n\tat ✽.I am on the homepage(C:/Users/rkeough/git/babson-automation/Include/features/Forms/Form321/Form321.feature:7)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "I am on the form \"https://www.babson.edu/grad/emss/part-time-graduate-programs/\" page",
  "keyword": "When "
});
formatter.match({
  "location": "Graduate152.FormPage(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input the first and last name on the form",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.FirstLastName()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input an email address",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputEmail()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a program \"tfa_1037\"",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectAProgramOfInterest(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a term \"tfa_1110\"",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectATermOfEnrollment(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input a phone number",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputPhoneNumber()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a country",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectACountry()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I am on the correct \"https://www.babson.edu/graduate/admissions/thank-you/\" page",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.thankYouPage(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I get the submit request",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.submitRequest()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I delete the form record",
  "keyword": "Then "
});
formatter.match({
  "location": "Graduate152.deleteRequest()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "name": "Submit program MBA form",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@445"
    },
    {
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "name": "I am on the homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "Graduate152.Homepage()"
});
formatter.result({
  "error_message": "com.kms.katalon.core.webui.exception.BrowserNotOpenedException: Browser is not opened\nBuild info: version: \u00273.141.59\u0027, revision: \u0027e82be7d358\u0027, time: \u00272018-11-14T08:25:53\u0027\nSystem info: host: \u0027L18-0032\u0027, ip: \u002710.0.0.226\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_181\u0027\nDriver info: driver.version: DriverFactory$getWebDriver\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.verifyWebDriverIsOpen(DriverFactory.java:985)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.verifyWebDriver(DriverFactory.java:970)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.getWebDriver(DriverFactory.java:956)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory$getWebDriver.call(Unknown Source)\r\n\tat forms.Graduate152.Homepage(Graduate152.groovy:49)\r\n\tat ✽.I am on the homepage(C:/Users/rkeough/git/babson-automation/Include/features/Forms/Form321/Form321.feature:7)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "I am on the form \"https://www.babson.edu/grad/emss/part-time-graduate-programs/\" page",
  "keyword": "When "
});
formatter.match({
  "location": "Graduate152.FormPage(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input the first and last name on the form",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.FirstLastName()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input an email address",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputEmail()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a program \"tfa_1037\"",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectAProgramOfInterest(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a term \"tfa_1368\"",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectATermOfEnrollment(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input a phone number",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputPhoneNumber()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a country",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectACountry()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I am on the correct \"https://www.babson.edu/graduate/admissions/thank-you/\" page",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.thankYouPage(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I get the submit request",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.submitRequest()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I delete the form record",
  "keyword": "Then "
});
formatter.match({
  "location": "Graduate152.deleteRequest()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "name": "Submit program MBA form",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@445"
    },
    {
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "name": "I am on the homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "Graduate152.Homepage()"
});
formatter.result({
  "error_message": "com.kms.katalon.core.webui.exception.BrowserNotOpenedException: Browser is not opened\nBuild info: version: \u00273.141.59\u0027, revision: \u0027e82be7d358\u0027, time: \u00272018-11-14T08:25:53\u0027\nSystem info: host: \u0027L18-0032\u0027, ip: \u002710.0.0.226\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_181\u0027\nDriver info: driver.version: DriverFactory$getWebDriver\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.verifyWebDriverIsOpen(DriverFactory.java:985)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.verifyWebDriver(DriverFactory.java:970)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.getWebDriver(DriverFactory.java:956)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory$getWebDriver.call(Unknown Source)\r\n\tat forms.Graduate152.Homepage(Graduate152.groovy:49)\r\n\tat ✽.I am on the homepage(C:/Users/rkeough/git/babson-automation/Include/features/Forms/Form321/Form321.feature:7)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "I am on the form \"https://www.babson.edu/grad/emss/part-time-graduate-programs/\" page",
  "keyword": "When "
});
formatter.match({
  "location": "Graduate152.FormPage(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input the first and last name on the form",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.FirstLastName()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input an email address",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputEmail()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a program \"tfa_1037\"",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectAProgramOfInterest(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a term \"tfa_1369\"",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectATermOfEnrollment(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input a phone number",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputPhoneNumber()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a country",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectACountry()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I am on the correct \"https://www.babson.edu/graduate/admissions/thank-you/\" page",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.thankYouPage(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I get the submit request",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.submitRequest()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I delete the form record",
  "keyword": "Then "
});
formatter.match({
  "location": "Graduate152.deleteRequest()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "name": "Submit program MBA form",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@445"
    },
    {
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "name": "I am on the homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "Graduate152.Homepage()"
});
formatter.result({
  "error_message": "com.kms.katalon.core.webui.exception.BrowserNotOpenedException: Browser is not opened\nBuild info: version: \u00273.141.59\u0027, revision: \u0027e82be7d358\u0027, time: \u00272018-11-14T08:25:53\u0027\nSystem info: host: \u0027L18-0032\u0027, ip: \u002710.0.0.226\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_181\u0027\nDriver info: driver.version: DriverFactory$getWebDriver\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.verifyWebDriverIsOpen(DriverFactory.java:985)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.verifyWebDriver(DriverFactory.java:970)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.getWebDriver(DriverFactory.java:956)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory$getWebDriver.call(Unknown Source)\r\n\tat forms.Graduate152.Homepage(Graduate152.groovy:49)\r\n\tat ✽.I am on the homepage(C:/Users/rkeough/git/babson-automation/Include/features/Forms/Form321/Form321.feature:7)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "I am on the form \"https://www.babson.edu/grad/emss/part-time-graduate-programs/\" page",
  "keyword": "When "
});
formatter.match({
  "location": "Graduate152.FormPage(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input the first and last name on the form",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.FirstLastName()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input an email address",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputEmail()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a program \"tfa_1037\"",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectAProgramOfInterest(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a term \"tfa_1399\"",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectATermOfEnrollment(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input a phone number",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputPhoneNumber()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a country",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectACountry()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I am on the correct \"https://www.babson.edu/graduate/admissions/thank-you/\" page",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.thankYouPage(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I get the submit request",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.submitRequest()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I delete the form record",
  "keyword": "Then "
});
formatter.match({
  "location": "Graduate152.deleteRequest()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "name": "Submit program MBA form",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@445"
    },
    {
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "name": "I am on the homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "Graduate152.Homepage()"
});
formatter.result({
  "error_message": "com.kms.katalon.core.webui.exception.BrowserNotOpenedException: Browser is not opened\nBuild info: version: \u00273.141.59\u0027, revision: \u0027e82be7d358\u0027, time: \u00272018-11-14T08:25:53\u0027\nSystem info: host: \u0027L18-0032\u0027, ip: \u002710.0.0.226\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_181\u0027\nDriver info: driver.version: DriverFactory$getWebDriver\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.verifyWebDriverIsOpen(DriverFactory.java:985)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.verifyWebDriver(DriverFactory.java:970)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.getWebDriver(DriverFactory.java:956)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory$getWebDriver.call(Unknown Source)\r\n\tat forms.Graduate152.Homepage(Graduate152.groovy:49)\r\n\tat ✽.I am on the homepage(C:/Users/rkeough/git/babson-automation/Include/features/Forms/Form321/Form321.feature:7)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "I am on the form \"https://www.babson.edu/grad/emss/part-time-graduate-programs/\" page",
  "keyword": "When "
});
formatter.match({
  "location": "Graduate152.FormPage(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input the first and last name on the form",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.FirstLastName()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input an email address",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputEmail()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a program \"tfa_1037\"",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectAProgramOfInterest(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a term \"tfa_1403\"",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectATermOfEnrollment(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input a phone number",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputPhoneNumber()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a country",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectACountry()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I am on the correct \"https://www.babson.edu/graduate/admissions/thank-you/\" page",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.thankYouPage(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I get the submit request",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.submitRequest()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I delete the form record",
  "keyword": "Then "
});
formatter.match({
  "location": "Graduate152.deleteRequest()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "name": "Submit program MBA form",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@445"
    },
    {
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "name": "I am on the homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "Graduate152.Homepage()"
});
formatter.result({
  "error_message": "com.kms.katalon.core.webui.exception.BrowserNotOpenedException: Browser is not opened\nBuild info: version: \u00273.141.59\u0027, revision: \u0027e82be7d358\u0027, time: \u00272018-11-14T08:25:53\u0027\nSystem info: host: \u0027L18-0032\u0027, ip: \u002710.0.0.226\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_181\u0027\nDriver info: driver.version: DriverFactory$getWebDriver\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.verifyWebDriverIsOpen(DriverFactory.java:985)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.verifyWebDriver(DriverFactory.java:970)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.getWebDriver(DriverFactory.java:956)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory$getWebDriver.call(Unknown Source)\r\n\tat forms.Graduate152.Homepage(Graduate152.groovy:49)\r\n\tat ✽.I am on the homepage(C:/Users/rkeough/git/babson-automation/Include/features/Forms/Form321/Form321.feature:7)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "I am on the form \"https://www.babson.edu/grad/emss/part-time-graduate-programs/\" page",
  "keyword": "When "
});
formatter.match({
  "location": "Graduate152.FormPage(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input the first and last name on the form",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.FirstLastName()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input an email address",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputEmail()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a program \"tfa_1037\"",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectAProgramOfInterest(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a term \"tfa_1426\"",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectATermOfEnrollment(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input a phone number",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputPhoneNumber()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a country",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectACountry()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I am on the correct \"https://www.babson.edu/graduate/admissions/thank-you/\" page",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.thankYouPage(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I get the submit request",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.submitRequest()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I delete the form record",
  "keyword": "Then "
});
formatter.match({
  "location": "Graduate152.deleteRequest()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "name": "Submit program MBA form",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@445"
    },
    {
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "name": "I am on the homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "Graduate152.Homepage()"
});
formatter.result({
  "error_message": "com.kms.katalon.core.webui.exception.BrowserNotOpenedException: Browser is not opened\nBuild info: version: \u00273.141.59\u0027, revision: \u0027e82be7d358\u0027, time: \u00272018-11-14T08:25:53\u0027\nSystem info: host: \u0027L18-0032\u0027, ip: \u002710.0.0.226\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_181\u0027\nDriver info: driver.version: DriverFactory$getWebDriver\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.verifyWebDriverIsOpen(DriverFactory.java:985)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.verifyWebDriver(DriverFactory.java:970)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.getWebDriver(DriverFactory.java:956)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory$getWebDriver.call(Unknown Source)\r\n\tat forms.Graduate152.Homepage(Graduate152.groovy:49)\r\n\tat ✽.I am on the homepage(C:/Users/rkeough/git/babson-automation/Include/features/Forms/Form321/Form321.feature:7)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "I am on the form \"https://www.babson.edu/grad/emss/part-time-graduate-programs/\" page",
  "keyword": "When "
});
formatter.match({
  "location": "Graduate152.FormPage(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input the first and last name on the form",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.FirstLastName()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input an email address",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputEmail()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a program \"tfa_1037\"",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectAProgramOfInterest(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a term \"tfa_1427\"",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectATermOfEnrollment(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input a phone number",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputPhoneNumber()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a country",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectACountry()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I am on the correct \"https://www.babson.edu/graduate/admissions/thank-you/\" page",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.thankYouPage(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I get the submit request",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.submitRequest()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I delete the form record",
  "keyword": "Then "
});
formatter.match({
  "location": "Graduate152.deleteRequest()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "name": "Submit program MBA form",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@445"
    },
    {
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "name": "I am on the homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "Graduate152.Homepage()"
});
formatter.result({
  "error_message": "com.kms.katalon.core.webui.exception.BrowserNotOpenedException: Browser is not opened\nBuild info: version: \u00273.141.59\u0027, revision: \u0027e82be7d358\u0027, time: \u00272018-11-14T08:25:53\u0027\nSystem info: host: \u0027L18-0032\u0027, ip: \u002710.0.0.226\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_181\u0027\nDriver info: driver.version: DriverFactory$getWebDriver\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.verifyWebDriverIsOpen(DriverFactory.java:985)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.verifyWebDriver(DriverFactory.java:970)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.getWebDriver(DriverFactory.java:956)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory$getWebDriver.call(Unknown Source)\r\n\tat forms.Graduate152.Homepage(Graduate152.groovy:49)\r\n\tat ✽.I am on the homepage(C:/Users/rkeough/git/babson-automation/Include/features/Forms/Form321/Form321.feature:7)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "I am on the form \"https://www.babson.edu/grad/emss/part-time-graduate-programs/\" page",
  "keyword": "When "
});
formatter.match({
  "location": "Graduate152.FormPage(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input the first and last name on the form",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.FirstLastName()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input an email address",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputEmail()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a program \"tfa_1043\"",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectAProgramOfInterest(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a term \"tfa_1099\"",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectATermOfEnrollment(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input a phone number",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputPhoneNumber()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a country",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectACountry()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I am on the correct \"https://www.babson.edu/graduate/admissions/thank-you/\" page",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.thankYouPage(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I get the submit request",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.submitRequest()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I delete the form record",
  "keyword": "Then "
});
formatter.match({
  "location": "Graduate152.deleteRequest()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "name": "Submit program MBA form",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@445"
    },
    {
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "name": "I am on the homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "Graduate152.Homepage()"
});
formatter.result({
  "error_message": "com.kms.katalon.core.webui.exception.BrowserNotOpenedException: Browser is not opened\nBuild info: version: \u00273.141.59\u0027, revision: \u0027e82be7d358\u0027, time: \u00272018-11-14T08:25:53\u0027\nSystem info: host: \u0027L18-0032\u0027, ip: \u002710.0.0.226\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_181\u0027\nDriver info: driver.version: DriverFactory$getWebDriver\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.verifyWebDriverIsOpen(DriverFactory.java:985)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.verifyWebDriver(DriverFactory.java:970)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.getWebDriver(DriverFactory.java:956)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory$getWebDriver.call(Unknown Source)\r\n\tat forms.Graduate152.Homepage(Graduate152.groovy:49)\r\n\tat ✽.I am on the homepage(C:/Users/rkeough/git/babson-automation/Include/features/Forms/Form321/Form321.feature:7)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "I am on the form \"https://www.babson.edu/grad/emss/part-time-graduate-programs/\" page",
  "keyword": "When "
});
formatter.match({
  "location": "Graduate152.FormPage(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input the first and last name on the form",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.FirstLastName()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input an email address",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputEmail()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a program \"tfa_1043\"",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectAProgramOfInterest(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a term \"tfa_1100\"",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectATermOfEnrollment(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input a phone number",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputPhoneNumber()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a country",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectACountry()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I am on the correct \"https://www.babson.edu/graduate/admissions/thank-you/\" page",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.thankYouPage(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I get the submit request",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.submitRequest()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I delete the form record",
  "keyword": "Then "
});
formatter.match({
  "location": "Graduate152.deleteRequest()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "name": "Submit program MBA form",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@445"
    },
    {
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "name": "I am on the homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "Graduate152.Homepage()"
});
formatter.result({
  "error_message": "com.kms.katalon.core.webui.exception.BrowserNotOpenedException: Browser is not opened\nBuild info: version: \u00273.141.59\u0027, revision: \u0027e82be7d358\u0027, time: \u00272018-11-14T08:25:53\u0027\nSystem info: host: \u0027L18-0032\u0027, ip: \u002710.0.0.226\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_181\u0027\nDriver info: driver.version: DriverFactory$getWebDriver\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.verifyWebDriverIsOpen(DriverFactory.java:985)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.verifyWebDriver(DriverFactory.java:970)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.getWebDriver(DriverFactory.java:956)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory$getWebDriver.call(Unknown Source)\r\n\tat forms.Graduate152.Homepage(Graduate152.groovy:49)\r\n\tat ✽.I am on the homepage(C:/Users/rkeough/git/babson-automation/Include/features/Forms/Form321/Form321.feature:7)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "I am on the form \"https://www.babson.edu/grad/emss/part-time-graduate-programs/\" page",
  "keyword": "When "
});
formatter.match({
  "location": "Graduate152.FormPage(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input the first and last name on the form",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.FirstLastName()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input an email address",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputEmail()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a program \"tfa_1043\"",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectAProgramOfInterest(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a term \"tfa_1101\"",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectATermOfEnrollment(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input a phone number",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputPhoneNumber()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a country",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectACountry()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I am on the correct \"https://www.babson.edu/graduate/admissions/thank-you/\" page",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.thankYouPage(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I get the submit request",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.submitRequest()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I delete the form record",
  "keyword": "Then "
});
formatter.match({
  "location": "Graduate152.deleteRequest()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "name": "Submit program MBA form",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@445"
    },
    {
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "name": "I am on the homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "Graduate152.Homepage()"
});
formatter.result({
  "error_message": "com.kms.katalon.core.webui.exception.BrowserNotOpenedException: Browser is not opened\nBuild info: version: \u00273.141.59\u0027, revision: \u0027e82be7d358\u0027, time: \u00272018-11-14T08:25:53\u0027\nSystem info: host: \u0027L18-0032\u0027, ip: \u002710.0.0.226\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_181\u0027\nDriver info: driver.version: DriverFactory$getWebDriver\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.verifyWebDriverIsOpen(DriverFactory.java:985)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.verifyWebDriver(DriverFactory.java:970)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.getWebDriver(DriverFactory.java:956)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory$getWebDriver.call(Unknown Source)\r\n\tat forms.Graduate152.Homepage(Graduate152.groovy:49)\r\n\tat ✽.I am on the homepage(C:/Users/rkeough/git/babson-automation/Include/features/Forms/Form321/Form321.feature:7)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "I am on the form \"https://www.babson.edu/grad/emss/part-time-graduate-programs/\" page",
  "keyword": "When "
});
formatter.match({
  "location": "Graduate152.FormPage(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input the first and last name on the form",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.FirstLastName()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input an email address",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputEmail()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a program \"tfa_1043\"",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectAProgramOfInterest(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a term \"tfa_1111\"",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectATermOfEnrollment(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input a phone number",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputPhoneNumber()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a country",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectACountry()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I am on the correct \"https://www.babson.edu/graduate/admissions/thank-you/\" page",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.thankYouPage(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I get the submit request",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.submitRequest()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I delete the form record",
  "keyword": "Then "
});
formatter.match({
  "location": "Graduate152.deleteRequest()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "name": "Submit program MBA form",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@445"
    },
    {
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "name": "I am on the homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "Graduate152.Homepage()"
});
formatter.result({
  "error_message": "com.kms.katalon.core.webui.exception.BrowserNotOpenedException: Browser is not opened\nBuild info: version: \u00273.141.59\u0027, revision: \u0027e82be7d358\u0027, time: \u00272018-11-14T08:25:53\u0027\nSystem info: host: \u0027L18-0032\u0027, ip: \u002710.0.0.226\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_181\u0027\nDriver info: driver.version: DriverFactory$getWebDriver\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.verifyWebDriverIsOpen(DriverFactory.java:985)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.verifyWebDriver(DriverFactory.java:970)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.getWebDriver(DriverFactory.java:956)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory$getWebDriver.call(Unknown Source)\r\n\tat forms.Graduate152.Homepage(Graduate152.groovy:49)\r\n\tat ✽.I am on the homepage(C:/Users/rkeough/git/babson-automation/Include/features/Forms/Form321/Form321.feature:7)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "I am on the form \"https://www.babson.edu/grad/emss/part-time-graduate-programs/\" page",
  "keyword": "When "
});
formatter.match({
  "location": "Graduate152.FormPage(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input the first and last name on the form",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.FirstLastName()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input an email address",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputEmail()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a program \"tfa_1043\"",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectAProgramOfInterest(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a term \"tfa_1370\"",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectATermOfEnrollment(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input a phone number",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputPhoneNumber()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a country",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectACountry()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I am on the correct \"https://www.babson.edu/graduate/admissions/thank-you/\" page",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.thankYouPage(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I get the submit request",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.submitRequest()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I delete the form record",
  "keyword": "Then "
});
formatter.match({
  "location": "Graduate152.deleteRequest()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "name": "Submit program MBA form",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@445"
    },
    {
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "name": "I am on the homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "Graduate152.Homepage()"
});
formatter.result({
  "error_message": "com.kms.katalon.core.webui.exception.BrowserNotOpenedException: Browser is not opened\nBuild info: version: \u00273.141.59\u0027, revision: \u0027e82be7d358\u0027, time: \u00272018-11-14T08:25:53\u0027\nSystem info: host: \u0027L18-0032\u0027, ip: \u002710.0.0.226\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_181\u0027\nDriver info: driver.version: DriverFactory$getWebDriver\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.verifyWebDriverIsOpen(DriverFactory.java:985)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.verifyWebDriver(DriverFactory.java:970)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.getWebDriver(DriverFactory.java:956)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory$getWebDriver.call(Unknown Source)\r\n\tat forms.Graduate152.Homepage(Graduate152.groovy:49)\r\n\tat ✽.I am on the homepage(C:/Users/rkeough/git/babson-automation/Include/features/Forms/Form321/Form321.feature:7)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "I am on the form \"https://www.babson.edu/grad/emss/part-time-graduate-programs/\" page",
  "keyword": "When "
});
formatter.match({
  "location": "Graduate152.FormPage(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input the first and last name on the form",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.FirstLastName()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input an email address",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputEmail()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a program \"tfa_1043\"",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectAProgramOfInterest(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a term \"tfa_1371\"",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectATermOfEnrollment(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input a phone number",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputPhoneNumber()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a country",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectACountry()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I am on the correct \"https://www.babson.edu/graduate/admissions/thank-you/\" page",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.thankYouPage(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I get the submit request",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.submitRequest()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I delete the form record",
  "keyword": "Then "
});
formatter.match({
  "location": "Graduate152.deleteRequest()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "name": "Submit program MBA form",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@445"
    },
    {
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "name": "I am on the homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "Graduate152.Homepage()"
});
formatter.result({
  "error_message": "com.kms.katalon.core.webui.exception.BrowserNotOpenedException: Browser is not opened\nBuild info: version: \u00273.141.59\u0027, revision: \u0027e82be7d358\u0027, time: \u00272018-11-14T08:25:53\u0027\nSystem info: host: \u0027L18-0032\u0027, ip: \u002710.0.0.226\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_181\u0027\nDriver info: driver.version: DriverFactory$getWebDriver\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.verifyWebDriverIsOpen(DriverFactory.java:985)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.verifyWebDriver(DriverFactory.java:970)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.getWebDriver(DriverFactory.java:956)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory$getWebDriver.call(Unknown Source)\r\n\tat forms.Graduate152.Homepage(Graduate152.groovy:49)\r\n\tat ✽.I am on the homepage(C:/Users/rkeough/git/babson-automation/Include/features/Forms/Form321/Form321.feature:7)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "I am on the form \"https://www.babson.edu/grad/emss/part-time-graduate-programs/\" page",
  "keyword": "When "
});
formatter.match({
  "location": "Graduate152.FormPage(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input the first and last name on the form",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.FirstLastName()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input an email address",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputEmail()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a program \"tfa_1043\"",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectAProgramOfInterest(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a term \"tfa_1372\"",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectATermOfEnrollment(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input a phone number",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputPhoneNumber()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a country",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectACountry()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I am on the correct \"https://www.babson.edu/graduate/admissions/thank-you/\" page",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.thankYouPage(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I get the submit request",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.submitRequest()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I delete the form record",
  "keyword": "Then "
});
formatter.match({
  "location": "Graduate152.deleteRequest()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "name": "Submit program MBA form",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@445"
    },
    {
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "name": "I am on the homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "Graduate152.Homepage()"
});
formatter.result({
  "error_message": "com.kms.katalon.core.webui.exception.BrowserNotOpenedException: Browser is not opened\nBuild info: version: \u00273.141.59\u0027, revision: \u0027e82be7d358\u0027, time: \u00272018-11-14T08:25:53\u0027\nSystem info: host: \u0027L18-0032\u0027, ip: \u002710.0.0.226\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_181\u0027\nDriver info: driver.version: DriverFactory$getWebDriver\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.verifyWebDriverIsOpen(DriverFactory.java:985)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.verifyWebDriver(DriverFactory.java:970)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.getWebDriver(DriverFactory.java:956)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory$getWebDriver.call(Unknown Source)\r\n\tat forms.Graduate152.Homepage(Graduate152.groovy:49)\r\n\tat ✽.I am on the homepage(C:/Users/rkeough/git/babson-automation/Include/features/Forms/Form321/Form321.feature:7)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "I am on the form \"https://www.babson.edu/grad/emss/part-time-graduate-programs/\" page",
  "keyword": "When "
});
formatter.match({
  "location": "Graduate152.FormPage(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input the first and last name on the form",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.FirstLastName()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input an email address",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputEmail()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a program \"tfa_1043\"",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectAProgramOfInterest(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a term \"tfa_1395\"",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectATermOfEnrollment(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input a phone number",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputPhoneNumber()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a country",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectACountry()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I am on the correct \"https://www.babson.edu/graduate/admissions/thank-you/\" page",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.thankYouPage(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I get the submit request",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.submitRequest()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I delete the form record",
  "keyword": "Then "
});
formatter.match({
  "location": "Graduate152.deleteRequest()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "name": "Submit program MBA form",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@445"
    },
    {
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "name": "I am on the homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "Graduate152.Homepage()"
});
formatter.result({
  "error_message": "com.kms.katalon.core.webui.exception.BrowserNotOpenedException: Browser is not opened\nBuild info: version: \u00273.141.59\u0027, revision: \u0027e82be7d358\u0027, time: \u00272018-11-14T08:25:53\u0027\nSystem info: host: \u0027L18-0032\u0027, ip: \u002710.0.0.226\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_181\u0027\nDriver info: driver.version: DriverFactory$getWebDriver\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.verifyWebDriverIsOpen(DriverFactory.java:985)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.verifyWebDriver(DriverFactory.java:970)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory.getWebDriver(DriverFactory.java:956)\r\n\tat com.kms.katalon.core.webui.driver.DriverFactory$getWebDriver.call(Unknown Source)\r\n\tat forms.Graduate152.Homepage(Graduate152.groovy:49)\r\n\tat ✽.I am on the homepage(C:/Users/rkeough/git/babson-automation/Include/features/Forms/Form321/Form321.feature:7)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "I am on the form \"https://www.babson.edu/grad/emss/part-time-graduate-programs/\" page",
  "keyword": "When "
});
formatter.match({
  "location": "Graduate152.FormPage(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input the first and last name on the form",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.FirstLastName()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input an email address",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputEmail()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a program \"tfa_1043\"",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectAProgramOfInterest(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a term \"tfa_1400\"",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectATermOfEnrollment(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I input a phone number",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.inputPhoneNumber()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I select a country",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.SelectACountry()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I am on the correct \"https://www.babson.edu/graduate/admissions/thank-you/\" page",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.thankYouPage(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I get the submit request",
  "keyword": "And "
});
formatter.match({
  "location": "Graduate152.submitRequest()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I delete the form record",
  "keyword": "Then "
});
formatter.match({
  "location": "Graduate152.deleteRequest()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "name": "Submit program MBA form",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@445"
    },
    {
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "name": "I am on the homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "Graduate152.Homepage()"
});
