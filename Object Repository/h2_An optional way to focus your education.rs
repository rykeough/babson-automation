<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h2_An optional way to focus your education</name>
   <tag></tag>
   <elementGuidId>566c0641-e883-4351-a30f-8f39481b4b91</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.text-long > h2</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='block-babson-portals-content']/article/div[2]/div/div/div/div/div/div/div/div/h2</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h2</value>
      <webElementGuid>8cabddaa-8ccb-460a-ac45-fc6dfb0e107d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>An optional way to focus your education.</value>
      <webElementGuid>7325701b-dc2b-427d-baaf-1a161b091c7a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;block-babson-portals-content&quot;)/article[@class=&quot;contextual-region&quot;]/div[2]/div[1]/div[1]/div[@class=&quot;paragraph paragraph--type--layout paragraph--view-mode--default&quot;]/div[@class=&quot;layout layout--base--onecol layout--base--onecol-- layout--background-attachment--default layout--background-position--center layout--background-size--cover layout--background-overlay--none layout--bottom-margin--default layout--top-bottom-padding--none layout--left-right-padding--none layout--container--default layout--content-container--default layout--column-gap--default layout--row-gap--default layout--column-breakpoint--medium layout--align-items--stretch layout--onecol&quot;]/div[@class=&quot;layout-content&quot;]/div[@class=&quot;layout__region layout__region--content&quot;]/div[@class=&quot;paragraph paragraph--type--general-content-block paragraph--view-mode--default margin-medium general-content-block--border-- general-content-block--&quot;]/div[@class=&quot;text-long&quot;]/h2[1]</value>
      <webElementGuid>addd7940-0ae8-448d-a1e5-a18915483cf9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='block-babson-portals-content']/article/div[2]/div/div/div/div/div/div/div/div/h2</value>
      <webElementGuid>e7bdc793-1f6e-4197-83e6-12b7514c820a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Delete'])[2]/following::h2[1]</value>
      <webElementGuid>1bc47e07-6f91-47a9-899a-8d0e93cbff73</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Edit'])[4]/following::h2[1]</value>
      <webElementGuid>3977238b-bb31-436d-8c4d-ca706c76346c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Overview'])[1]/preceding::h2[1]</value>
      <webElementGuid>21146cb7-af43-46ab-a458-1a1fd311b32a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Concentrations are four courses (14-16 credits)'])[1]/preceding::h2[1]</value>
      <webElementGuid>3c1cbd3c-8d72-4f83-bbba-6613bc281f9f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='An optional way to focus your education.']/parent::*</value>
      <webElementGuid>59bc14dd-7500-49b8-850a-631a14653d30</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/h2</value>
      <webElementGuid>b4c9c33b-2fe0-4b46-8ccb-175d7bac5cf7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h2[(text() = 'An optional way to focus your education.' or . = 'An optional way to focus your education.')]</value>
      <webElementGuid>f8e5a2cf-3987-4787-aac3-ed7eda40235d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
