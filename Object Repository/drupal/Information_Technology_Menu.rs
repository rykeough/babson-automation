<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Information_Technology_Menu</name>
   <tag></tag>
   <elementGuidId>2c9b96e1-cb3b-456b-9455-a0fdf8ca53fb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@href = '/it' and (text() = 'Information Technology' or . = 'Information Technology')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='block-itlink']/div[2]/div/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.it-homepage > a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>931f4c19-a946-49f2-bd2b-3a3e46dcffb1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/it</value>
      <webElementGuid>1545a1a7-8860-4e7f-bbdb-4277219c946b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Information Technology</value>
      <webElementGuid>31e32479-ce71-4738-bf20-f6d352c34928</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;block-itlink&quot;)/div[@class=&quot;text-long&quot;]/div[@class=&quot;it-homepage&quot;]/a[1]</value>
      <webElementGuid>00b87eb7-81ca-4957-a595-21759432539a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='block-itlink']/div[2]/div/a</value>
      <webElementGuid>97910fd4-f971-4cda-b128-6d975c5e1f09</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Information Technology')]</value>
      <webElementGuid>d6fea552-1461-4693-8d6a-de5ff9008791</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Clone'])[8]/following::a[1]</value>
      <webElementGuid>592afb5f-8294-46e9-b203-91d356b5d8db</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Edit'])[2]/following::a[2]</value>
      <webElementGuid>f1a92a38-fc5a-4ffe-870a-5c70c93e4a51</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Open Breadcrumb configuration options'])[1]/preceding::a[2]</value>
      <webElementGuid>002c62a4-b85f-4b9e-afba-d9658d42e18a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Configure block'])[6]/preceding::a[2]</value>
      <webElementGuid>73b20d84-7146-4c57-a2b4-491cc20576bc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '/it')])[6]</value>
      <webElementGuid>6012584a-041b-43e6-938a-80ff4f301f57</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div[2]/div/a</value>
      <webElementGuid>3c345c71-cdaa-44dc-b871-a531bcc515c4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/it' and (text() = 'Information Technology' or . = 'Information Technology')]</value>
      <webElementGuid>92a9ea2b-ba9a-4126-a921-4774a2020cd5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
