<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>DrupalLogin</name>
   <tag></tag>
   <elementGuidId>7812c5b4-04c7-40c6-9496-a178c068c1f2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@type = 'submit' and @id = 'edit-submit' and @name = 'op']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='edit-submit']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#edit-submit</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>74f6c6b8-688f-402c-8d08-c0adedef1a45</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-drupal-selector</name>
      <type>Main</type>
      <value>edit-submit</value>
      <webElementGuid>87ce5c04-c476-4177-b37a-64c3cdb073f2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>3c87db0f-6893-4e4a-9bc4-58ffdbfdc6ee</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>edit-submit</value>
      <webElementGuid>4e3d470c-61fd-434c-97b5-768c0d46dde9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>op</value>
      <webElementGuid>7090f6cd-b15b-45b2-b2c7-52b5c542e444</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Log in</value>
      <webElementGuid>0c922c99-3dac-4832-a266-28b2d4f7983e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>button js-form-submit form-submit form-item__textfield</value>
      <webElementGuid>f6247e0f-60da-429b-8bc5-511abdf9dac1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;edit-submit&quot;)</value>
      <webElementGuid>845a2891-3f8f-4a4d-8a03-1750b0bec545</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='edit-submit']</value>
      <webElementGuid>ec99169e-a2e0-4b58-b312-817600667e69</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='edit-actions']/input</value>
      <webElementGuid>0a7c1bf3-21bc-410d-aaea-044794321f30</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/input</value>
      <webElementGuid>ff33558d-3edd-4d57-a2b1-4be87c18afa3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'submit' and @id = 'edit-submit' and @name = 'op']</value>
      <webElementGuid>ef369bc0-bc6c-4704-900b-e077e95ee921</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
