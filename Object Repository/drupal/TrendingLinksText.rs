<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>TrendingLinksText</name>
   <tag></tag>
   <elementGuidId>d2195748-0b83-4c82-a695-59b11a838b30</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Trending Links' or . = 'Trending Links')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='block-babson-portals-content']/article/div[2]/div/div[2]/div/div/div/div/div/div/div/h3</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.trending-link-section.section-one > h3</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h3</value>
      <webElementGuid>514dd609-cc77-4662-abdd-1de89ffd12b5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Trending Links</value>
      <webElementGuid>4a121f5c-7929-47cf-a696-b17d97a23783</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;block-babson-portals-content&quot;)/article[@class=&quot;contextual-region&quot;]/div[2]/div[1]/div[2]/div[@class=&quot;paragraph paragraph--type--layout paragraph--view-mode--default&quot;]/div[@class=&quot;layout layout--base--twocols layout--base--twocols-- layout--background-attachment--default layout--background-position--center layout--background-size--cover layout--background-overlay--none layout--bottom-margin--big layout--top-bottom-padding--none layout--left-right-padding--none layout--container--default layout--content-container--default layout--column-gap--default layout--row-gap--default layout--column-breakpoint--medium layout--column-width--default layout--align-items--stretch layout-builder-base--two-columns layout--twocol&quot;]/div[@class=&quot;layout-content&quot;]/div[@class=&quot;layout__region layout__region--first&quot;]/div[@class=&quot;paragraph paragraph--type--general-content-block paragraph--view-mode--default add-padding add-drop-shadow general-content-block--border--mango-punch general-content-block--white&quot;]/div[@class=&quot;text-long&quot;]/div[@class=&quot;trending-link-section section-one&quot;]/h3[1]</value>
      <webElementGuid>42e8db22-0443-44bc-951b-93278a56c4c3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='block-babson-portals-content']/article/div[2]/div/div[2]/div/div/div/div/div/div/div/h3</value>
      <webElementGuid>ed2951f4-3abd-4b0b-8a6e-dff35a96ab12</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search'])[2]/following::h3[1]</value>
      <webElementGuid>87a7a508-7978-45fd-a155-35b64de96a3f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='People Search'])[3]/following::h3[1]</value>
      <webElementGuid>3b446917-e92e-4c73-9418-65782b8be638</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Academic Calendar'])[1]/preceding::h3[1]</value>
      <webElementGuid>43199ccd-08db-4f31-93d7-8d45e406240a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Canvas logo'])[1]/preceding::h3[1]</value>
      <webElementGuid>d8fdc7eb-583b-4d86-af65-e862217b15ce</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Trending Links']/parent::*</value>
      <webElementGuid>a43dbb74-1b47-403c-9523-db14670a673d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/h3</value>
      <webElementGuid>f919e453-1903-4ea2-84c5-e494e13b9747</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h3[(text() = 'Trending Links' or . = 'Trending Links')]</value>
      <webElementGuid>c66ee312-3822-4ccd-9975-b0cd01a9223e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
