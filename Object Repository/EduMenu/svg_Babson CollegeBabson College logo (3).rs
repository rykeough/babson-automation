<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>svg_Babson CollegeBabson College logo (3)</name>
   <tag></tag>
   <elementGuidId>f7b8ba93-6782-418c-9eae-4a092b49f706</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>svg</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Main Content'])[1]/following::*[name()='svg'][1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>svg</value>
      <webElementGuid>9998dc42-463a-48c2-a1e3-ee552c0742e2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>viewBox</name>
      <type>Main</type>
      <value>0 0 356 43</value>
      <webElementGuid>30dc5b0f-c246-48e9-8770-0a4e4d95bca7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>img</value>
      <webElementGuid>762c1cc7-4e10-466f-b682-896912e14c64</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-labelledby</name>
      <type>Main</type>
      <value>titleID descID</value>
      <webElementGuid>cb69223e-cd67-429a-a05a-51b0ee8134f9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Babson CollegeBabson College logo</value>
      <webElementGuid>5495a7a3-e8a3-453d-8263-f6ece830a0ce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;simple-page-layout pl-2022 fonts-loaded&quot;]/body[@class=&quot;no-js section-id-47294&quot;]/div[@class=&quot;c-header__wrapper fixed&quot;]/header[@class=&quot;c-header&quot;]/div[@class=&quot;c-branding&quot;]/div[@class=&quot;c-logo&quot;]/a[1]/svg[1]</value>
      <webElementGuid>40e39033-4623-4531-b9be-4ef6b03c4932</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Main Content'])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>3430879e-dc26-4e5f-96d7-442a59d8afb4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Main Navigation'])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>156b1c17-f5ad-49cc-9e24-0878fcca3239</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Professional'])[1]/preceding::*[name()='svg'][1]</value>
      <webElementGuid>fc432d51-e071-4ace-918c-96314bb0aa8d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//svg[(text() = 'Babson CollegeBabson College logo' or . = 'Babson CollegeBabson College logo')]</value>
      <webElementGuid>3fcbb08b-3a08-4ed0-953b-7e74fbe1430f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
