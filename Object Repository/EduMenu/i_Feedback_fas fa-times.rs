<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>i_Feedback_fas fa-times</name>
   <tag></tag>
   <elementGuidId>ccbee036-3735-4020-9daf-f4bece495efa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.boxclose > i.fas.fa-times</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='cookie-handler']/div/div/button/i</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
      <webElementGuid>17e5e586-db08-45a5-bfb4-995aecb31e94</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fas fa-times</value>
      <webElementGuid>e34f9365-a400-4241-888b-5f720aa8a60e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-hidden</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>a6d4c792-0d1b-4280-9dfd-910efc1e54bf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;cookie-handler&quot;)/div[1]/div[@class=&quot;boxclose-wrapper&quot;]/button[@class=&quot;boxclose&quot;]/i[@class=&quot;fas fa-times&quot;]</value>
      <webElementGuid>f64cef9e-11f0-4e6b-bd75-4a9b3f5ed707</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='cookie-handler']/div/div/button/i</value>
      <webElementGuid>89aad90c-822b-4f8f-8a56-4c04c1b83a9e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/button/i</value>
      <webElementGuid>cc5a68d4-550f-4268-a94c-285ac31a1a62</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
