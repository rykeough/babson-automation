<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>svg_Babson CollegeBabson College logo</name>
   <tag></tag>
   <elementGuidId>f4a16027-4a98-4771-a347-b4729a62c255</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>svg</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Main Content'])[1]/following::*[name()='svg'][1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>svg</value>
      <webElementGuid>766e88dc-0e1d-4e24-8a70-1c0030658b53</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>viewBox</name>
      <type>Main</type>
      <value>0 0 356 43</value>
      <webElementGuid>55b18210-8113-42e9-b06c-ff362324b715</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>img</value>
      <webElementGuid>e1a0c694-fb0c-4c91-bd25-9b27c2f4412c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-labelledby</name>
      <type>Main</type>
      <value>titleID descID</value>
      <webElementGuid>55268418-1f8b-45a5-a235-81eaedde5ec6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Babson CollegeBabson College logo</value>
      <webElementGuid>ad10c31f-1c70-4a0d-9249-c716b41e43fb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;full-width-page-layout pl-2022 fonts-loaded&quot;]/body[@class=&quot;section-id-48141&quot;]/div[@class=&quot;c-header__wrapper fixed&quot;]/header[@class=&quot;c-header&quot;]/div[@class=&quot;c-branding&quot;]/div[@class=&quot;c-logo&quot;]/a[1]/svg[1]</value>
      <webElementGuid>eba0fff1-62b6-4dee-9b69-9440885d2b20</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Main Content'])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>132aeaf8-68e9-43d3-8639-b91b72865f20</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Main Navigation'])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>8c9e6545-7bda-4270-be14-6ab2b6cc0dd3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Arthur M. Blank School for Entrepreneurial Leadership'])[1]/preceding::*[name()='svg'][1]</value>
      <webElementGuid>2699e18e-61c3-4b38-acae-668a05493dc3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//svg[(text() = 'Babson CollegeBabson College logo' or . = 'Babson CollegeBabson College logo')]</value>
      <webElementGuid>2ce9c4db-546a-49ea-b85b-517607c63194</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
