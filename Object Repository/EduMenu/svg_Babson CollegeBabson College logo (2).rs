<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>svg_Babson CollegeBabson College logo (2)</name>
   <tag></tag>
   <elementGuidId>0c2b2f84-afcd-49f6-a2a3-7a9b6b22b892</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>svg</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Main Content'])[1]/following::*[name()='svg'][1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>svg</value>
      <webElementGuid>b3aa6a55-371c-49fe-a78b-12d3d5b97859</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>viewBox</name>
      <type>Main</type>
      <value>0 0 356 43</value>
      <webElementGuid>cd1dd128-021c-4047-ace1-517bcae9f796</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>img</value>
      <webElementGuid>5f14497b-a350-4001-8bd1-169a2c10526e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-labelledby</name>
      <type>Main</type>
      <value>titleID descID</value>
      <webElementGuid>62a372d9-b7f9-45c2-9212-3d92dd099b06</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Babson CollegeBabson College logo</value>
      <webElementGuid>e6d826e4-eed6-44ea-b75a-869d08a92202</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;simple-page-layout pl-2022 fonts-loaded&quot;]/body[@class=&quot;no-js section-id-49200&quot;]/div[@class=&quot;c-header__wrapper fixed&quot;]/header[@class=&quot;c-header&quot;]/div[@class=&quot;c-branding&quot;]/div[@class=&quot;c-logo&quot;]/a[1]/svg[1]</value>
      <webElementGuid>ea217123-1598-4c49-88ce-e103769daf45</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Main Content'])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>ca8018ff-babb-4639-8811-686fe438d660</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Main Navigation'])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>4db74ab3-9d34-4d91-93c1-e0177616b128</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Graduate'])[1]/preceding::*[name()='svg'][1]</value>
      <webElementGuid>8b9c1b51-d590-4a61-b858-e53e193b9641</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//svg[(text() = 'Babson CollegeBabson College logo' or . = 'Babson CollegeBabson College logo')]</value>
      <webElementGuid>b72a3724-e41e-48fe-87cd-e11a8439d37d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
