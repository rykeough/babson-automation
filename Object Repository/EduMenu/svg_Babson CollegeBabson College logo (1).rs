<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>svg_Babson CollegeBabson College logo (1)</name>
   <tag></tag>
   <elementGuidId>102f03e7-c23e-4631-bd59-5a30aed44450</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>svg</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Main Content'])[1]/following::*[name()='svg'][1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>svg</value>
      <webElementGuid>26121c54-e066-4ab7-a4ea-cfb8735d807d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>viewBox</name>
      <type>Main</type>
      <value>0 0 356 43</value>
      <webElementGuid>d397648e-d9e8-4a7c-97c1-02f25e1dfe20</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>img</value>
      <webElementGuid>163815a2-e06f-46df-8fff-4fa6da07dcbb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-labelledby</name>
      <type>Main</type>
      <value>titleID descID</value>
      <webElementGuid>65b7304a-e289-4ee1-9578-fbfb927bd4d9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Babson CollegeBabson College logo</value>
      <webElementGuid>bbd76fce-2865-416c-99ef-7904eeade001</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;simple-page-layout pl-2022 fonts-loaded no-touch&quot;]/body[@class=&quot;no-js section-id-48452&quot;]/div[@class=&quot;c-header__wrapper fixed&quot;]/header[@class=&quot;c-header&quot;]/div[@class=&quot;c-branding&quot;]/div[@class=&quot;c-logo&quot;]/a[1]/svg[1]</value>
      <webElementGuid>400814f0-6d45-46b9-99d8-49691d599215</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Main Content'])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>7926e237-ad39-41c1-93f9-bd37dc988127</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Main Navigation'])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>59fd5a72-a7c5-44a5-b9b2-d52bc9abd07a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Undergraduate'])[1]/preceding::*[name()='svg'][1]</value>
      <webElementGuid>6f1c2e8a-7563-4bba-9e8a-646a8717438f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//svg[(text() = 'Babson CollegeBabson College logo' or . = 'Babson CollegeBabson College logo')]</value>
      <webElementGuid>18223ca6-c1d1-4e7a-9913-6639233ad19f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
