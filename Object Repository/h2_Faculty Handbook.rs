<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h2_Faculty Handbook</name>
   <tag></tag>
   <elementGuidId>2e930ed0-fd4c-4623-a431-802d2a0d4161</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.text-long > h2</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='block-babson-portals-content']/article/div[2]/div/div/div/div/div/div/div/div/h2</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h2</value>
      <webElementGuid>120c503b-31d9-43f3-aeed-a6ee5eca1670</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Faculty Handbook</value>
      <webElementGuid>7e48d993-f15e-4a18-9441-6708d632008c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;block-babson-portals-content&quot;)/article[@class=&quot;contextual-region&quot;]/div[2]/div[1]/div[1]/div[@class=&quot;paragraph paragraph--type--layout paragraph--view-mode--default&quot;]/div[@class=&quot;layout layout--base--onecol layout--base--onecol-- layout--background-attachment--default layout--background-position--center layout--background-size--cover layout--background-overlay--none layout--bottom-margin--default layout--top-bottom-padding--none layout--left-right-padding--none layout--container--default layout--content-container--default layout--column-gap--default layout--row-gap--default layout--column-breakpoint--medium layout--align-items--stretch layout--onecol&quot;]/div[@class=&quot;layout-content&quot;]/div[@class=&quot;layout__region layout__region--content&quot;]/div[@class=&quot;paragraph paragraph--type--general-content-block paragraph--view-mode--default margin-medium general-content-block--border-- general-content-block--&quot;]/div[@class=&quot;text-long&quot;]/h2[1]</value>
      <webElementGuid>040c121a-5af4-4410-a004-0eabe02ca49f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='block-babson-portals-content']/article/div[2]/div/div/div/div/div/div/div/div/h2</value>
      <webElementGuid>060c804a-5076-4ec7-a1a9-4323a84bf13e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Delete'])[2]/following::h2[1]</value>
      <webElementGuid>e0f292de-7905-4652-8a67-020c87708c8a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Edit'])[4]/following::h2[1]</value>
      <webElementGuid>8c4009aa-26d1-4ea0-a799-0ca69853d962</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='DeanofFaculty@babson.edu'])[1]/preceding::h2[1]</value>
      <webElementGuid>17d58357-a8e2-42b6-b291-0d3a47d8c192</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Intranet'])[2]/preceding::h2[1]</value>
      <webElementGuid>480dc541-b7f4-4412-a69b-7c9884fc5b4a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/h2</value>
      <webElementGuid>818bfd9f-ca33-48b5-a10c-00fa3ea63f2a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h2[(text() = 'Faculty Handbook' or . = 'Faculty Handbook')]</value>
      <webElementGuid>3fdba14e-6721-42e5-83b0-a7a99df8574d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
