<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h2_Digital Measures Overview</name>
   <tag></tag>
   <elementGuidId>d251a922-fbc4-4859-a50d-de67e391925c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.text-long > h2</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='block-babson-portals-content']/article/div[2]/div/div/div/div/div/div/div/div/h2</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h2</value>
      <webElementGuid>124835f9-bf4b-46ec-a796-543d363b7b47</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Digital Measures Overview</value>
      <webElementGuid>020d7e21-fbb1-4343-954e-71f2ac67d9ce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;block-babson-portals-content&quot;)/article[@class=&quot;contextual-region&quot;]/div[2]/div[1]/div[1]/div[@class=&quot;paragraph paragraph--type--layout paragraph--view-mode--default&quot;]/div[@class=&quot;layout layout--base--onecol layout--base--onecol-- layout--background-attachment--default layout--background-position--center layout--background-size--cover layout--background-overlay--none layout--bottom-margin--default layout--top-bottom-padding--none layout--left-right-padding--none layout--container--default layout--content-container--default layout--column-gap--default layout--row-gap--default layout--column-breakpoint--medium layout--align-items--stretch layout--onecol&quot;]/div[@class=&quot;layout-content&quot;]/div[@class=&quot;layout__region layout__region--content&quot;]/div[@class=&quot;paragraph paragraph--type--general-content-block paragraph--view-mode--default margin-medium general-content-block--border-- general-content-block--&quot;]/div[@class=&quot;text-long&quot;]/h2[1]</value>
      <webElementGuid>ec291279-b5ae-47d5-8bb9-f89e414febff</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='block-babson-portals-content']/article/div[2]/div/div/div/div/div/div/div/div/h2</value>
      <webElementGuid>4a2c55ef-e8af-44d3-a3d9-04bc09433543</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Click here'])[1]/following::h2[1]</value>
      <webElementGuid>29270052-8cbe-4e10-b1ff-9054bd0ae842</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Delete'])[2]/following::h2[1]</value>
      <webElementGuid>1e3140ec-a846-49cf-b895-675a1a62bf51</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='For additional videos and guides, visit the left navigation bar in the Hub.'])[1]/preceding::h2[1]</value>
      <webElementGuid>43f06107-d2bb-43a1-a155-f995f4a813b3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Need Help?'])[1]/preceding::h2[1]</value>
      <webElementGuid>b7ffa435-0d25-41ff-a056-bb5380dbc17f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Digital Measures Overview']/parent::*</value>
      <webElementGuid>f87a5696-e054-4360-b3b7-956bc2e2d8ca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/h2</value>
      <webElementGuid>6580d547-4988-42b6-b778-44209cd96bd4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h2[(text() = 'Digital Measures Overview' or . = 'Digital Measures Overview')]</value>
      <webElementGuid>eb702239-0ddf-4f23-895a-e00829431f85</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
