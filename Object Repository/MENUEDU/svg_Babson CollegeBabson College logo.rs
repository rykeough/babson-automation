<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>svg_Babson CollegeBabson College logo</name>
   <tag></tag>
   <elementGuidId>b58f8800-2fe8-4f17-9da9-da9a692ad938</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Main Content'])[1]/following::*[name()='svg'][1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>svg</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>svg</value>
      <webElementGuid>887def1f-662a-47b4-8548-ed67de12bc17</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>viewBox</name>
      <type>Main</type>
      <value>0 0 356 43</value>
      <webElementGuid>159625ca-1894-4495-9287-5ab435b6ca86</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>img</value>
      <webElementGuid>8a643d55-ee82-4093-91a1-c7fc506746f3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-labelledby</name>
      <type>Main</type>
      <value>titleID descID</value>
      <webElementGuid>cb583f25-09ed-49da-ada1-5531c8e7f65f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Babson CollegeBabson College logo</value>
      <webElementGuid>49815987-1555-49ae-bc4a-79396689515e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;simple-page-layout pl-2022 fonts-loaded&quot;]/body[@class=&quot;no-js section-id-50133&quot;]/div[@class=&quot;c-header__wrapper fixed&quot;]/header[@class=&quot;c-header&quot;]/div[@class=&quot;c-branding&quot;]/div[@class=&quot;c-logo&quot;]/a[1]/svg[1]</value>
      <webElementGuid>4f8656be-a8ba-4ada-b6e6-0814a505e4f8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Main Content'])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>c9292e31-2a73-4d09-aeef-8331b5b932ce</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Main Navigation'])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>891104da-dea0-4508-9f92-a1ec1dca3706</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Explore Babson'])[1]/preceding::*[name()='svg'][1]</value>
      <webElementGuid>e488dde4-2bb5-4139-80cc-c34367f4c53f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//svg[(text() = 'Babson CollegeBabson College logo' or . = 'Babson CollegeBabson College logo')]</value>
      <webElementGuid>60055f11-83b2-462c-a640-d5a8173dc5cc</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
