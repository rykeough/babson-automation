<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h3_App Launcher</name>
   <tag></tag>
   <elementGuidId>16f6a685-fa75-47b5-9836-3d6fc0faaa60</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='block-babson-portals-content']/article/div/div/div[3]/div/div/div/div/div/div/div/h3</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>h3</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h3</value>
      <webElementGuid>5c40842b-6e26-4b89-997e-101888eac670</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>App Launcher</value>
      <webElementGuid>83aa11ba-64a9-4530-8f99-83725a2e3c08</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;block-babson-portals-content&quot;)/article[1]/div[1]/div[1]/div[3]/div[@class=&quot;paragraph paragraph--type--layout paragraph--view-mode--default&quot;]/div[@class=&quot;layout layout--base--twocols layout--base--twocols-- layout--background-attachment--default layout--background-position--center layout--background-size--cover layout--background-overlay--none layout--bottom-margin--default layout--top-bottom-padding--none layout--left-right-padding--none layout--container--default layout--content-container--default layout--column-gap--default layout--row-gap--default layout--column-breakpoint--medium layout--column-width--default layout--align-items--stretch layout-builder-base--two-columns layout--twocol&quot;]/div[@class=&quot;layout-content&quot;]/div[@class=&quot;layout__region layout__region--first&quot;]/div[@class=&quot;paragraph paragraph--type--general-content-block paragraph--view-mode--default add-padding add-drop-shadow margin-medium general-content-block--border--mango-punch general-content-block--&quot;]/div[@class=&quot;text-long&quot;]/div[@class=&quot;trending-link-section section-one&quot;]/h3[1]</value>
      <webElementGuid>3e66a02e-9462-4be6-b790-adb8aea43ecd</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='block-babson-portals-content']/article/div/div/div[3]/div/div/div/div/div/div/div/h3</value>
      <webElementGuid>aa62e9ca-ba3a-417c-a21d-2bdfcee3bcc6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search'])[1]/following::h3[1]</value>
      <webElementGuid>a5ac77e5-320c-4d17-af93-87939ab8a421</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='People Search'])[2]/following::h3[1]</value>
      <webElementGuid>4da8b2b3-db1f-4061-a12c-5cf227d55256</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Academic Alert Forms'])[3]/preceding::h3[1]</value>
      <webElementGuid>c16f1178-a3e7-4f52-ae8f-2b21c4c7cf6f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Academic Calendar'])[1]/preceding::h3[1]</value>
      <webElementGuid>96080044-3609-47ad-9a03-7e3e380a0764</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='App Launcher']/parent::*</value>
      <webElementGuid>0c42b7c6-6976-495e-af9c-58d001365416</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h3</value>
      <webElementGuid>9b7c5d63-844c-4d76-b86a-2fa83c53de43</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h3[(text() = 'App Launcher' or . = 'App Launcher')]</value>
      <webElementGuid>30c4bc56-cb52-476f-a1c9-9b2428175243</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
