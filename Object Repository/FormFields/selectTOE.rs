<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>selectTOE</name>
   <tag></tag>
   <elementGuidId>4529a650-9667-47b3-a222-fb2221adf8f4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'tfa_8' and @name = 'tfa_8' and @title = 'Intended Term of Enrollment' and (text() = 'Please select...

Summer 2022
 Fall 2022
 Summer 2023
 Fall 2023
 Summer 2024
 Fall 2024
 Summer 2025
 Fall 2025


 Fall 2021
 Spring 2022
 Summer 2022
 Fall 2022
 Spring 2023
 Summer 2023
 Fall 2023
 Spring 2024
 Summer 2024
 Fall 2024
 Spring 2025
 Summer 2025


 Fall 2021
 Spring 2022
 Summer 2022
 Fall 2022
 Spring 2023
 Summer 2023
 Fall 2023
 Spring 2024
 Summer 2024
 Fall 2024
 Spring 2025
 Summer 2025


 Summer 2022
 Summer 2023
 Summer 2024
 Summer 2025


 Fall 2022
 Fall 2023
 Fall 2024
 Fall 2025


Fall 2021
Fall 2022
 Fall 2023
 Fall 2024


Fall 2022
 Fall 2023
 Fall 2024
 Fall 2025


Fall 2022
 Fall 2023
 Fall 2024
 Fall 2025


Fall 2022
 Fall 2023
 Fall 2024
 Fall 2025


 Fall 2021
 Spring 2022
 Fall 2022
 Spring 2023
 Fall 2023
 Spring 2024
 Fall 2024
 Spring 2025


 Fall 2021
 Spring 2022
 Summer 2022
 Fall 2022
 Spring 2023
 Summer 2023
 Fall 2023
 Spring 2024
 Summer 2024
 Fall 2024
 Spring 2025
 Summer 2025


 Fall 2021
 Spring 2022
 Summer 2022
 Fall 2022
 Spring 2023
 Summer 2023
 Fall 2023
 Spring 2024
 Summer 2024
 Fall 2024
 Spring 2025
 Summer 2025
' or . = 'Please select...

Summer 2022
 Fall 2022
 Summer 2023
 Fall 2023
 Summer 2024
 Fall 2024
 Summer 2025
 Fall 2025


 Fall 2021
 Spring 2022
 Summer 2022
 Fall 2022
 Spring 2023
 Summer 2023
 Fall 2023
 Spring 2024
 Summer 2024
 Fall 2024
 Spring 2025
 Summer 2025


 Fall 2021
 Spring 2022
 Summer 2022
 Fall 2022
 Spring 2023
 Summer 2023
 Fall 2023
 Spring 2024
 Summer 2024
 Fall 2024
 Spring 2025
 Summer 2025


 Summer 2022
 Summer 2023
 Summer 2024
 Summer 2025


 Fall 2022
 Fall 2023
 Fall 2024
 Fall 2025


Fall 2021
Fall 2022
 Fall 2023
 Fall 2024


Fall 2022
 Fall 2023
 Fall 2024
 Fall 2025


Fall 2022
 Fall 2023
 Fall 2024
 Fall 2025


Fall 2022
 Fall 2023
 Fall 2024
 Fall 2025


 Fall 2021
 Spring 2022
 Fall 2022
 Spring 2023
 Fall 2023
 Spring 2024
 Fall 2024
 Spring 2025


 Fall 2021
 Spring 2022
 Summer 2022
 Fall 2022
 Spring 2023
 Summer 2023
 Fall 2023
 Spring 2024
 Summer 2024
 Fall 2024
 Spring 2025
 Summer 2025


 Fall 2021
 Spring 2022
 Summer 2022
 Fall 2022
 Spring 2023
 Summer 2023
 Fall 2023
 Spring 2024
 Summer 2024
 Fall 2024
 Spring 2025
 Summer 2025
')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#tfa_8</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='tfa_8']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>77c1ee10-d65c-476c-ae53-945104479fd7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-required</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>77c4fb6d-1c4a-48c6-9a9f-141a0136646a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>tfa_8</value>
      <webElementGuid>a791ddfd-d3e2-4822-a3c0-bed4d7632e07</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>tfa_8</value>
      <webElementGuid>5d3ed8dd-fe0e-4a24-a81b-11de0e1dbcdd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-filter-control</name>
      <type>Main</type>
      <value>#tfa_4</value>
      <webElementGuid>f8a0da02-91b5-442e-b588-41810c59d17c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Intended Term of Enrollment</value>
      <webElementGuid>f3e7e6a6-7438-4404-b74c-825fafaed91d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>required</value>
      <webElementGuid>a26ae565-cb6a-4039-82a0-7d5013116d70</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Please select...

Summer 2022
 Fall 2022
 Summer 2023
 Fall 2023
 Summer 2024
 Fall 2024
 Summer 2025
 Fall 2025


 Fall 2021
 Spring 2022
 Summer 2022
 Fall 2022
 Spring 2023
 Summer 2023
 Fall 2023
 Spring 2024
 Summer 2024
 Fall 2024
 Spring 2025
 Summer 2025


 Fall 2021
 Spring 2022
 Summer 2022
 Fall 2022
 Spring 2023
 Summer 2023
 Fall 2023
 Spring 2024
 Summer 2024
 Fall 2024
 Spring 2025
 Summer 2025


 Summer 2022
 Summer 2023
 Summer 2024
 Summer 2025


 Fall 2022
 Fall 2023
 Fall 2024
 Fall 2025


Fall 2021
Fall 2022
 Fall 2023
 Fall 2024


Fall 2022
 Fall 2023
 Fall 2024
 Fall 2025


Fall 2022
 Fall 2023
 Fall 2024
 Fall 2025


Fall 2022
 Fall 2023
 Fall 2024
 Fall 2025


 Fall 2021
 Spring 2022
 Fall 2022
 Spring 2023
 Fall 2023
 Spring 2024
 Fall 2024
 Spring 2025


 Fall 2021
 Spring 2022
 Summer 2022
 Fall 2022
 Spring 2023
 Summer 2023
 Fall 2023
 Spring 2024
 Summer 2024
 Fall 2024
 Spring 2025
 Summer 2025


 Fall 2021
 Spring 2022
 Summer 2022
 Fall 2022
 Spring 2023
 Summer 2023
 Fall 2023
 Spring 2024
 Summer 2024
 Fall 2024
 Spring 2025
 Summer 2025
</value>
      <webElementGuid>e4c82c40-1769-4d50-afa4-4b60c40fa690</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;tfa_8&quot;)</value>
      <webElementGuid>6c441a59-573b-4464-9c40-754e65293845</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='tfa_8']</value>
      <webElementGuid>da04a0d6-bc3c-410c-9225-09df06c419a0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='tfa_8-D']/div/select</value>
      <webElementGuid>db9fa4a1-6cd8-4d41-b809-5d866af13bb9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Intended Term of Enrollment'])[1]/following::select[1]</value>
      <webElementGuid>b8b31aa2-ade9-4d2c-8275-31294d30f838</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Program of Interest'])[1]/following::select[2]</value>
      <webElementGuid>34858510-8821-4930-9ea4-59403ae3c61c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Phone'])[1]/preceding::select[1]</value>
      <webElementGuid>cc72589c-8df8-4acf-8250-5c8f4225419f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='International students, include your country code'])[1]/preceding::select[1]</value>
      <webElementGuid>9bb1cbb0-7f88-46eb-9391-692f7e502723</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/select</value>
      <webElementGuid>014096f7-09f3-4384-ba44-2cc2bedd66fe</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
