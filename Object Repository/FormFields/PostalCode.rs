<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>PostalCode</name>
   <tag></tag>
   <elementGuidId>660ded07-5641-4490-b236-36c2338050a5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@title = 'Postal Code']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Postal Code</value>
      <webElementGuid>780f3a5f-b2c4-48a1-a92a-e02004e9cf75</webElementGuid>
   </webElementProperties>
</WebElementEntity>
