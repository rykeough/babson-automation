<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>selectPOI</name>
   <tag></tag>
   <elementGuidId>a5226e52-862f-477b-9492-622437b3f026</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'tfa_4' and @name = 'tfa_4' and @title = 'Program of Interest' and (text() = 'Please select...
Full-Time MBA
Part-Time MBA
Online MBA
One-Year MBA
Two-Year MBA
Blended Learning MBA - Miami
Master in Entrepreneurial Leadership
Master in Advanced Entrepreneurial Leadership
Master of Science in Finance
Master of Science in Business Analytics - Wellesley
Certificate in Advanced Management
Undecided' or . = 'Please select...
Full-Time MBA
Part-Time MBA
Online MBA
One-Year MBA
Two-Year MBA
Blended Learning MBA - Miami
Master in Entrepreneurial Leadership
Master in Advanced Entrepreneurial Leadership
Master of Science in Finance
Master of Science in Business Analytics - Wellesley
Certificate in Advanced Management
Undecided')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#tfa_4</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='tfa_4']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>1089d0d9-9d93-4221-a9f7-4057c258c1eb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-required</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>76f2d15e-9400-47ea-b402-1b5087e992a4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>tfa_4</value>
      <webElementGuid>75958789-c09b-4cc7-b388-01e889a899fb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>tfa_4</value>
      <webElementGuid>28074756-3cc9-44aa-b684-6402c9f1d21d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-filter-dependent</name>
      <type>Main</type>
      <value>#tfa_8</value>
      <webElementGuid>cb797fc1-9bb9-4022-a745-fa04cce7b342</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Program of Interest</value>
      <webElementGuid>b2af4793-d1bf-4777-acf5-a0762e57f404</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>required</value>
      <webElementGuid>1c527d16-19e2-42ac-ac5b-713de26ddb88</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Please select...
Full-Time MBA
Part-Time MBA
Online MBA
One-Year MBA
Two-Year MBA
Blended Learning MBA - Miami
Master in Entrepreneurial Leadership
Master in Advanced Entrepreneurial Leadership
Master of Science in Finance
Master of Science in Business Analytics - Wellesley
Certificate in Advanced Management
Undecided</value>
      <webElementGuid>1fb2fc6e-9ffd-45b1-9e44-0e50c3cb7237</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;tfa_4&quot;)</value>
      <webElementGuid>d401f6eb-434c-4e71-9b45-481acd4704ef</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='tfa_4']</value>
      <webElementGuid>8b89b93c-609f-40e1-9c63-95cb7a221731</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='tfa_4-D']/div/select</value>
      <webElementGuid>7ad222b3-2866-4413-8bdb-97106fd9eef8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Program of Interest'])[1]/following::select[1]</value>
      <webElementGuid>32a5ef7b-99c9-4ced-b6e3-cb96ab01a1e7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email'])[1]/following::select[1]</value>
      <webElementGuid>55205695-c0ee-4534-973f-ab7a5d42c0fb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Intended Term of Enrollment'])[1]/preceding::select[1]</value>
      <webElementGuid>b7f9e76d-6e90-438f-b764-7fd2e70e53fa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Phone'])[1]/preceding::select[2]</value>
      <webElementGuid>ff0e117f-1d88-4e92-ad11-8715c6fc81b5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//select</value>
      <webElementGuid>8e2c7dc9-33b3-4adb-8bf0-05356ba182f6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
