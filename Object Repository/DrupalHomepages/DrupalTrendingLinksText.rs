<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>DrupalTrendingLinksText</name>
   <tag></tag>
   <elementGuidId>6c09848b-c5db-447b-9cf2-5fe73d3883b3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.trending-link-section.section-one > h3</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='block-babson-portals-content']/article/div/div/div[2]/div/div/div/div/div/div/div/h3</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h3</value>
      <webElementGuid>3ef01182-6975-4579-bed8-58f82e37a1bf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Trending Links</value>
      <webElementGuid>88358281-dfaa-47b2-9def-f35dfe190138</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;block-babson-portals-content&quot;)/article[1]/div[1]/div[1]/div[2]/div[@class=&quot;paragraph paragraph--type--layout paragraph--view-mode--default&quot;]/div[@class=&quot;layout layout--base--twocols layout--base--twocols-- layout--background-attachment--default layout--background-position--center layout--background-size--cover layout--background-overlay--none layout--bottom-margin--big layout--top-bottom-padding--none layout--left-right-padding--none layout--container--default layout--content-container--default layout--column-gap--default layout--row-gap--default layout--column-breakpoint--medium layout--column-width--default layout--align-items--stretch layout-builder-base--two-columns layout--twocol&quot;]/div[@class=&quot;layout-content&quot;]/div[@class=&quot;layout__region layout__region--first&quot;]/div[@class=&quot;paragraph paragraph--type--general-content-block paragraph--view-mode--default add-padding add-drop-shadow general-content-block--border--mango-punch general-content-block--white&quot;]/div[@class=&quot;text-long&quot;]/div[@class=&quot;trending-link-section section-one&quot;]/h3[1]</value>
      <webElementGuid>e363c0ac-5cdf-43e2-985d-067672a42fca</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='block-babson-portals-content']/article/div/div/div[2]/div/div/div/div/div/div/div/h3</value>
      <webElementGuid>87e414c2-e7fa-4675-9c27-7952d46d4e6a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search'])[2]/following::h3[1]</value>
      <webElementGuid>46b27aa8-8c1c-466d-95f4-fbf106fa5cda</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='People Search'])[3]/following::h3[1]</value>
      <webElementGuid>20269a5f-262b-4895-aaa9-f8fe41ca43e4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Academic Calendar'])[1]/preceding::h3[1]</value>
      <webElementGuid>11542855-61c7-42a5-9184-32d63c6ff078</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Canvas logo'])[1]/preceding::h3[1]</value>
      <webElementGuid>8e4d11ea-1de8-4f06-a69c-2dc5f773862e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Trending Links']/parent::*</value>
      <webElementGuid>918b1d76-766b-472d-a67e-830eb92cfd7c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/h3</value>
      <webElementGuid>77cf298d-91c8-4677-a2ee-8faef7acc079</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h3[(text() = 'Trending Links' or . = 'Trending Links')]</value>
      <webElementGuid>a808483a-c8e2-4958-827c-3fd8646c4e4b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
