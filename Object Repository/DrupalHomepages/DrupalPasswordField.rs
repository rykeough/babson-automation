<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>DrupalPasswordField</name>
   <tag></tag>
   <elementGuidId>7ba6122e-8d43-49d6-81f4-b1e11f002581</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#edit-pass</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='edit-pass']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>3787bdd5-6495-4036-b7d8-9a59ebbdc27e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>current-password</value>
      <webElementGuid>d0c0b4c5-f001-4d5d-beb6-f3deb425ffc0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-drupal-selector</name>
      <type>Main</type>
      <value>edit-pass</value>
      <webElementGuid>8fbb71a4-a1de-4c0c-9407-e88c558b23b4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>be637fe4-6fda-43e0-b746-bd143ae4bb06</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>edit-pass</value>
      <webElementGuid>2fc47426-bc53-4c20-988d-cd19d7ad5e3f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>pass</value>
      <webElementGuid>91b794e0-5c57-4eb4-be5c-6aa9bc91a1eb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>size</name>
      <type>Main</type>
      <value>60</value>
      <webElementGuid>5f7872b3-5ccc-439e-9300-ad66ae0e84cd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>128</value>
      <webElementGuid>266c30e1-2525-4fea-b53d-9796c0fd4e27</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-text required form-item__textfield</value>
      <webElementGuid>95e85943-fa99-4523-b364-dd908dbd08c9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>required</name>
      <type>Main</type>
      <value>required</value>
      <webElementGuid>e519eafb-e4b0-4033-8400-44d9cae3b9a4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-required</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>41e991e8-5ce9-44e8-82ba-76c30f67e7bc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;edit-pass&quot;)</value>
      <webElementGuid>c2030909-59b3-4d09-bba8-31614e924ea9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='edit-pass']</value>
      <webElementGuid>55cec05e-e44f-4ab6-968c-6b8e78ac2b4f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='user-login-form']/div[2]/input</value>
      <webElementGuid>b4e76d44-4ed7-463a-b8bb-fdff3f2b23e5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/input</value>
      <webElementGuid>a81b5087-a784-4af3-8697-4f6034a9645f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'password' and @id = 'edit-pass' and @name = 'pass']</value>
      <webElementGuid>39807be1-fe62-4c71-a20e-1a9dbc6e4b7d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
