<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h3_Welcome to Babson Colleges Office of Res_84bc66</name>
   <tag></tag>
   <elementGuidId>67d8ef3e-3fd0-46cf-a846-c7c62aecf976</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.text-long > h3</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='block-babson-portals-content']/article/div[2]/div/div/div/div/div/div/div/div/h3</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h3</value>
      <webElementGuid>d58c6807-e561-48c9-b406-16ec4f445cf0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Welcome to Babson College's Office of Residence Life!</value>
      <webElementGuid>270b4525-d82e-4aa2-b196-5822505b2583</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;block-babson-portals-content&quot;)/article[@class=&quot;contextual-region&quot;]/div[2]/div[1]/div[1]/div[@class=&quot;paragraph paragraph--type--layout paragraph--view-mode--default&quot;]/div[@class=&quot;layout layout--base--onecol layout--base--onecol-- layout--background-attachment--default layout--background-position--center layout--background-size--cover layout--background-overlay--none layout--bottom-margin--none layout--top-bottom-padding--none layout--left-right-padding--none layout--container--default layout--content-container--default layout--onecol&quot;]/div[@class=&quot;layout-content&quot;]/div[@class=&quot;layout__region layout__region--content&quot;]/div[@class=&quot;paragraph paragraph--type--general-content-block paragraph--view-mode--default margin-medium general-content-block--border-- general-content-block--&quot;]/div[@class=&quot;text-long&quot;]/h3[1]</value>
      <webElementGuid>30de2cde-ae7b-4ead-98b6-69ed039c2315</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='block-babson-portals-content']/article/div[2]/div/div/div/div/div/div/div/div/h3</value>
      <webElementGuid>8708764a-cc7a-4d0c-bcd2-06748fedceca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Delete'])[2]/following::h3[1]</value>
      <webElementGuid>fccf315e-97f9-4cf1-8363-c51beae8c1af</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Edit'])[4]/following::h3[1]</value>
      <webElementGuid>6949baf0-d753-4deb-b47c-66413df2c76c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='WHO WE ARE'])[1]/preceding::h3[1]</value>
      <webElementGuid>0f0fcab9-7e83-4542-9fe1-514e885d7e3c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/h3</value>
      <webElementGuid>ba52e1bc-663e-484d-8184-390a92dbfb7a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h3[(text() = concat(&quot;Welcome to Babson College&quot; , &quot;'&quot; , &quot;s Office of Residence Life!&quot;) or . = concat(&quot;Welcome to Babson College&quot; , &quot;'&quot; , &quot;s Office of Residence Life!&quot;))]</value>
      <webElementGuid>9351194d-84c4-4899-914f-bec19607ba22</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
