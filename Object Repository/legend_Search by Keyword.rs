<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>legend_Search by Keyword</name>
   <tag></tag>
   <elementGuidId>e11fcb50-d007-4286-b286-25586263f5e4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='searchoptions-generic']/div/div/form/div/fieldset/legend</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>legend</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>legend</value>
      <webElementGuid>5f6f2649-51d6-46a2-b156-785975abcf27</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Search by Keyword</value>
      <webElementGuid>8f67493c-4404-47cf-ae3c-d26a07d34bcc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;searchoptions-generic&quot;)/div[@class=&quot;row percent-row search-filter-block&quot;]/div[@class=&quot;large-6 small-12&quot;]/form[1]/div[1]/fieldset[@class=&quot;panel&quot;]/legend[1]</value>
      <webElementGuid>1cb1e63e-ecb6-4966-bb53-230cc4dd71dd</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='searchoptions-generic']/div/div/form/div/fieldset/legend</value>
      <webElementGuid>c8be39b6-9d0f-4744-9dd9-7e74ff6c478d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All'])[1]/following::legend[1]</value>
      <webElementGuid>778df269-5dd8-4337-b3bf-3725718e9e10</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Year'])[1]/following::legend[1]</value>
      <webElementGuid>c00877f7-afde-400c-aca2-1a232323cd15</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search:'])[1]/preceding::legend[1]</value>
      <webElementGuid>e9c15dd2-03f9-4825-8d38-3e7218050607</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Filter for events:'])[1]/preceding::legend[1]</value>
      <webElementGuid>5f0e025b-d00e-4a5f-bb1b-6defdc621513</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Search by Keyword']/parent::*</value>
      <webElementGuid>9149d55c-a554-4cc5-8d60-5d892dd8fac6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//legend</value>
      <webElementGuid>abf85405-5137-432f-976d-fcf092f3ee32</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//legend[(text() = 'Search by Keyword' or . = 'Search by Keyword')]</value>
      <webElementGuid>30190ae6-82d2-4546-8e76-db47c6ba5b9b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
