<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h2_Student Employment Philosophy</name>
   <tag></tag>
   <elementGuidId>5ce1ef86-607a-49a4-ba1b-bf227a75f369</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.text-long > h2</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='block-babson-portals-content']/article/div[2]/div/div/div/div/div/div/div/div/h2</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h2</value>
      <webElementGuid>5473dd98-6bb6-455d-b335-cb9270ea6ad4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Student Employment Philosophy</value>
      <webElementGuid>56af470f-f2e3-43d9-95c3-cb33d29811de</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;block-babson-portals-content&quot;)/article[@class=&quot;contextual-region&quot;]/div[2]/div[1]/div[1]/div[@class=&quot;paragraph paragraph--type--layout paragraph--view-mode--default&quot;]/div[@class=&quot;layout layout--base--onecol layout--base--onecol-- layout--background-attachment--default layout--background-position--center layout--background-size--cover layout--background-overlay--none layout--bottom-margin--default layout--top-bottom-padding--none layout--left-right-padding--none layout--container--default layout--content-container--default layout--column-gap--default layout--row-gap--default layout--column-breakpoint--medium layout--align-items--stretch layout--onecol&quot;]/div[@class=&quot;layout-content&quot;]/div[@class=&quot;layout__region layout__region--content&quot;]/div[@class=&quot;paragraph paragraph--type--general-content-block paragraph--view-mode--default margin-medium general-content-block--border-- general-content-block--&quot;]/div[@class=&quot;text-long&quot;]/h2[1]</value>
      <webElementGuid>2f44e383-c3d9-4dfd-a052-ba53bb5711c6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='block-babson-portals-content']/article/div[2]/div/div/div/div/div/div/div/div/h2</value>
      <webElementGuid>04b9af3e-c96c-4413-ba67-eba85b235c2a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Delete'])[2]/following::h2[1]</value>
      <webElementGuid>531746ae-8bc5-40a9-855e-ff12f0d43494</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Edit'])[4]/following::h2[1]</value>
      <webElementGuid>a7e058b1-5363-41ee-962c-6dd56fd5730a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Create employment opportunities from which students can gain work experience;'])[1]/preceding::h2[1]</value>
      <webElementGuid>2c100657-2dd3-4cff-b6e8-cda5b12b27ec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Provide students with a means to help meet their educational expenses;'])[1]/preceding::h2[1]</value>
      <webElementGuid>b8abfa38-1be5-4de5-876d-cc77fec3abd5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Student Employment Philosophy']/parent::*</value>
      <webElementGuid>84e5b820-cdef-4f31-ab54-f6f6664206a0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/h2</value>
      <webElementGuid>e132a35e-8d92-49c7-9b0e-83796d3bb277</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h2[(text() = 'Student Employment Philosophy' or . = 'Student Employment Philosophy')]</value>
      <webElementGuid>525143ab-e7d1-4297-8943-507b66108857</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
