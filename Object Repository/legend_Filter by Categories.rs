<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>legend_Filter by Categories</name>
   <tag></tag>
   <elementGuidId>585a584a-9f6e-4d0f-867b-4ab72283b2b5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='searchoptions-generic']/div/div[2]/div/fieldset/legend</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.search-filter > fieldset > legend</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>legend</value>
      <webElementGuid>d735eec5-b1d1-4c5a-8fdb-046afc7d60e8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Filter by Categories</value>
      <webElementGuid>8b726eba-157c-4301-9688-fe6f8ef32c3e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;searchoptions-generic&quot;)/div[@class=&quot;row percent-row search-filter-block&quot;]/div[@class=&quot;large-6 small-12&quot;]/div[@class=&quot;search-filter&quot;]/fieldset[1]/legend[1]</value>
      <webElementGuid>b810b198-b9bc-42ce-a70e-344bc0035180</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='searchoptions-generic']/div/div[2]/div/fieldset/legend</value>
      <webElementGuid>3304d9d7-1e87-464c-a0fa-dd3aeb3940d4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Undergraduate Students'])[1]/following::legend[1]</value>
      <webElementGuid>60c0c9c2-3267-4a64-b0c2-71da852014b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Undergraduate CCD Fairs'])[1]/following::legend[1]</value>
      <webElementGuid>3899a744-ba49-44bd-b0d2-32718967e2e9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Categories'])[1]/preceding::legend[1]</value>
      <webElementGuid>35b6a48f-6d44-4d3a-99cc-bd7e37740687</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sub Category'])[1]/preceding::legend[1]</value>
      <webElementGuid>6497cf32-fa9d-4afa-9a5c-92ffb8c21eab</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Filter by Categories']/parent::*</value>
      <webElementGuid>a7686b5c-4374-43e3-a176-6e6d4c503ce4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/fieldset/legend</value>
      <webElementGuid>0e9d85c5-2b67-4d98-8434-3beadc7a6bea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//legend[(text() = 'Filter by Categories' or . = 'Filter by Categories')]</value>
      <webElementGuid>91eeda9f-3b5b-4746-9fc6-7e24b6dc006c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
