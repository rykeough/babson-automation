<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_New Students Graduate</name>
   <tag></tag>
   <elementGuidId>06d5f96c-8d17-4b57-8e60-f36e5dd5a522</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h1 > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='block-babson-portals-page-title']/h1/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>79e89843-08e5-463c-ba8f-3d41f4c00678</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>New Students: Graduate</value>
      <webElementGuid>4f308378-92c6-4192-af69-0c12a470d31c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;block-babson-portals-page-title&quot;)/h1[1]/span[1]</value>
      <webElementGuid>28224e01-b7b6-41c2-9622-89d6a3dcf7e8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='block-babson-portals-page-title']/h1/span</value>
      <webElementGuid>64c12f13-060f-425d-b395-48352e661470</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Clone'])[16]/following::span[1]</value>
      <webElementGuid>9496f393-68db-4b8f-8573-73c04066b227</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Remove block'])[10]/following::span[1]</value>
      <webElementGuid>d3e0f195-0980-4116-89fc-c5be8018ff7c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Open Primary tabs configuration options'])[1]/preceding::span[1]</value>
      <webElementGuid>041e6089-f9d1-458d-9800-a6700e91c6de</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Configure block'])[11]/preceding::span[1]</value>
      <webElementGuid>e7e97564-83a9-4a53-afd2-1b7206e85ed5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='New Students: Graduate']/parent::*</value>
      <webElementGuid>3b083c8f-575d-4d89-9465-edaf1e2817c8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h1/span</value>
      <webElementGuid>815ea089-e840-405f-bfe0-cab03ef6c5e7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'New Students: Graduate' or . = 'New Students: Graduate')]</value>
      <webElementGuid>658250e6-98f6-426a-abd9-9eb724b4e96a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
