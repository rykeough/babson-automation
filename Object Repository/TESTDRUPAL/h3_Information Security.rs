<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h3_Information Security</name>
   <tag></tag>
   <elementGuidId>bc349064-b080-4e66-aa12-d609a9307c17</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='block-babson-portals-content']/article/div[2]/div/div[2]/div/div/div/div[2]/div/div/div/h3</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.announcements-section > h3</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h3</value>
      <webElementGuid>2b47d115-fa6e-46a5-b18a-cb0bade58855</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Information Security</value>
      <webElementGuid>c5994993-40c3-4e7c-a4a1-c0411b94e04b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;block-babson-portals-content&quot;)/article[@class=&quot;contextual-region&quot;]/div[2]/div[1]/div[2]/div[@class=&quot;paragraph paragraph--type--layout paragraph--view-mode--default&quot;]/div[@class=&quot;layout layout--base--twocols layout--base--twocols-- layout--background-attachment--default layout--background-position--center layout--background-size--cover layout--background-overlay--none layout--bottom-margin--default layout--top-bottom-padding--none layout--left-right-padding--none layout--container--default layout--content-container--default layout--column-gap--default layout--row-gap--default layout--column-breakpoint--small layout--column-width--default layout--align-items--stretch layout-builder-base--two-columns layout--twocol&quot;]/div[@class=&quot;layout-content&quot;]/div[@class=&quot;layout__region layout__region--second&quot;]/div[@class=&quot;paragraph paragraph--type--general-content-block paragraph--view-mode--default add-padding add-drop-shadow margin-medium general-content-block--border--mango-punch general-content-block--&quot;]/div[@class=&quot;text-long&quot;]/div[@class=&quot;announcements-section&quot;]/h3[1]</value>
      <webElementGuid>82ccb91c-3aae-408f-9cad-672bb475e897</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='block-babson-portals-content']/article/div[2]/div/div[2]/div/div/div/div[2]/div/div/div/h3</value>
      <webElementGuid>d9eddd5a-c358-4e19-99d5-7e4a888e6a9f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='See IT News and How-To Articles on Knowledge'])[1]/following::h3[1]</value>
      <webElementGuid>ee2876c7-da85-425b-828d-27773aad1815</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Predictive Model for Math, along with a Math Tutorbot to improve academic outcomes'])[1]/following::h3[1]</value>
      <webElementGuid>751e1fe9-ccc3-46d2-a9b6-fb4399103a90</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='phishing attempts'])[1]/preceding::h3[1]</value>
      <webElementGuid>6def246f-403b-4638-8fdc-efc52f363512</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Information Security newsletter'])[1]/preceding::h3[1]</value>
      <webElementGuid>a19d3ccd-d37d-4b6d-a170-8057fabff7c9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Information Security']/parent::*</value>
      <webElementGuid>a5aea7c6-611e-4768-92d0-2ceca35e1530</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/h3</value>
      <webElementGuid>90c29f96-3c2d-4e58-bbb2-5761a70712fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h3[(text() = 'Information Security' or . = 'Information Security')]</value>
      <webElementGuid>56491ab0-3024-4c00-a7bc-d0cfe141dc05</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
