<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Federated login_op</name>
   <tag></tag>
   <elementGuidId>a2059754-1944-40b9-8490-a67459740603</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='edit-submit']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#edit-submit</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>c4b641bf-8896-4d08-9309-1aa6ff6e4ad4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-drupal-selector</name>
      <type>Main</type>
      <value>edit-submit</value>
      <webElementGuid>d8ea54dd-4d3f-40dc-ad86-373c87be0e16</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>20a9d4b4-868c-4828-a067-c9219d734348</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>edit-submit</value>
      <webElementGuid>763b333f-4993-45a0-a2e3-946553546881</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>op</value>
      <webElementGuid>1ee9741c-e0bc-49fa-925c-ed59a6e6f8f2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Log in</value>
      <webElementGuid>e801d8e9-561b-443e-9528-b5ad296e4573</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>button js-form-submit form-submit form-item__textfield</value>
      <webElementGuid>2e519d61-4902-4945-a6b9-4fea5804577e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;edit-submit&quot;)</value>
      <webElementGuid>6a2496f0-8553-47ad-a930-e78b895efdf0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='edit-submit']</value>
      <webElementGuid>eb7eab08-297c-4923-867e-204125366a04</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='edit-actions']/input</value>
      <webElementGuid>444d9c17-fa12-407e-a21a-d8940c332944</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/input</value>
      <webElementGuid>e4481ed7-9c2b-4512-b88d-10bdbcc33c9a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'submit' and @id = 'edit-submit' and @name = 'op']</value>
      <webElementGuid>8d3b575b-b835-4282-9026-e17b9f65281a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
