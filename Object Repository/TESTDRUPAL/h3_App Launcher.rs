<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h3_App Launcher</name>
   <tag></tag>
   <elementGuidId>36ce3d99-b0a5-4136-b8d0-f7d424a208ba</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='block-babson-portals-content']/article/div[2]/div/div[3]/div/div/div/div/div/div/div/h3</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.trending-link-section.section-one > h3</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h3</value>
      <webElementGuid>7aec3be7-58af-42a3-8719-b45946282eaf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>App Launcher</value>
      <webElementGuid>747b6b37-a3e6-4ca4-86bc-bff234b67b7c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;block-babson-portals-content&quot;)/article[@class=&quot;contextual-region&quot;]/div[2]/div[1]/div[3]/div[@class=&quot;paragraph paragraph--type--layout paragraph--view-mode--default&quot;]/div[@class=&quot;layout layout--base--twocols layout--base--twocols-- layout--background-attachment--default layout--background-position--center layout--background-size--cover layout--background-overlay--none layout--bottom-margin--default layout--top-bottom-padding--none layout--left-right-padding--none layout--container--default layout--content-container--default layout--column-gap--default layout--row-gap--default layout--column-breakpoint--medium layout--column-width--default layout--align-items--stretch layout-builder-base--two-columns layout--twocol&quot;]/div[@class=&quot;layout-content&quot;]/div[@class=&quot;layout__region layout__region--first&quot;]/div[@class=&quot;paragraph paragraph--type--general-content-block paragraph--view-mode--default add-padding add-drop-shadow margin-medium general-content-block--border--mango-punch general-content-block--&quot;]/div[@class=&quot;text-long&quot;]/div[@class=&quot;trending-link-section section-one&quot;]/h3[1]</value>
      <webElementGuid>a9bc100e-20d7-4f78-a170-55e23648db41</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='block-babson-portals-content']/article/div[2]/div/div[3]/div/div/div/div/div/div/div/h3</value>
      <webElementGuid>32953828-e25d-4ea3-800f-f1f3e8062904</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search'])[1]/following::h3[1]</value>
      <webElementGuid>f8204d61-9ceb-4924-99b3-d38673be8c59</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='People Search'])[2]/following::h3[1]</value>
      <webElementGuid>0bdc5e8e-5e6a-4b4f-b493-e4042ba45558</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Academic Alert Forms'])[3]/preceding::h3[1]</value>
      <webElementGuid>125d22dc-60f6-4928-bdf1-9fb8e923ad8d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Academic Calendar'])[1]/preceding::h3[1]</value>
      <webElementGuid>db2acfa7-4cb3-4691-9e01-4c699224ffad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='App Launcher']/parent::*</value>
      <webElementGuid>ac0d4191-7141-4453-b3bd-e6b69f35a0fa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/h3</value>
      <webElementGuid>dd942940-95e4-4df8-82c1-c12ffbc10180</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h3[(text() = 'App Launcher' or . = 'App Launcher')]</value>
      <webElementGuid>6b2e0244-9f6f-4a4a-b8e7-643abc86a120</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
