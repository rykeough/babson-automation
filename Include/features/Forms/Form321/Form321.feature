@445
Feature: Form 321
  I want to be able to fill out fields of the forms and submit

  @smoke
  Scenario Outline: Submit program MBA form
    Given I am on the homepage
    When I am on the form "<form>" page
    And I input the first and last name on the form
    And I input an email address
    And I select a program "<program>"
    And I select a term "<term>"
    And I input a phone number
    And I select a country
    And I am on the correct "<thankYou>" page
    And I get the submit request
    Then I delete the form record

    Examples: 
      | form                                                          | program  | term     | thankYou                                              |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1037 | tfa_1093 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1037 | tfa_1110 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1037 | tfa_1368 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1037 | tfa_1369 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1037 | tfa_1399 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1037 | tfa_1403 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1037 | tfa_1426 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1037 | tfa_1427 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1043 | tfa_1099 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1043 | tfa_1100 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1043 | tfa_1101 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1043 | tfa_1111 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1043 | tfa_1370 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1043 | tfa_1371 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1043 | tfa_1372 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1043 | tfa_1395 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1043 | tfa_1400 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1043 | tfa_1412 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1116 | tfa_1375 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1116 | tfa_1376 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1116 | tfa_1377 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1116 | tfa_1378 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1116 | tfa_1379 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1116 | tfa_1380 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1116 | tfa_1381 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1116 | tfa_1396 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1116 | tfa_1401 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1116 | tfa_1413 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1116 | tfa_1428 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1057 | tfa_1112 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1057 | tfa_1382 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1057 | tfa_1404 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1057 | tfa_1431 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1063 | tfa_1113 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1063 | tfa_1383 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1063 | tfa_1405 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1063 | tfa_1425 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1069 | tfa_1114 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1069 | tfa_1384 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1069 | tfa_1406 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1069 | tfa_1432 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1075 | tfa_1107 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1075 | tfa_1108 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1075 | tfa_1109 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1075 | tfa_1115 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1075 | tfa_1392 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1075 | tfa_1393 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1075 | tfa_1394 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1075 | tfa_1398 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1075 | tfa_1402 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1075 | tfa_1414 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1407 | tfa_1410 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1407 | tfa_1411 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1407 | tfa_1429 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/grad/emss/part-time-graduate-programs/ | tfa_1407 | tfa_1430 | https://www.babson.edu/graduate/admissions/thank-you/ |
