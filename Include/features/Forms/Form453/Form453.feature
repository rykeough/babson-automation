@Form @453 @Graduate
Feature: babson.edu/graduate
  I want to submit and verify form submits with the correct data

  @Grad @152 @smoke
  Scenario Outline: Submit graduate form
    Given I am on the homepage
    When I am on the form "<form>" page
    And I input the first and last name on the form
    And I input an email address
    And I select a program "<program>"
    And I select a term "<term>"
    And I input a phone number
    And I select a country
    And I input a state
    And I input a city
    And I input the postal code
    And I click the submit button
    And I am on the correct "<thankYou>" page
    And I get the submit request
    Then I delete the form record

    Examples: 
      | form                             | program  | term     | thankYou                                              |
      | https://www.babson.edu/graduate/ | tfa_1184 | tfa_1186 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1184 | tfa_1196 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1184 | tfa_1219 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1184 | tfa_1220 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1184 | tfa_1257 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1184 | tfa_1263 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1184 | tfa_1272 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1184 | tfa_1273 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1043 | tfa_1103 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1043 | tfa_1187 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1043 | tfa_1188 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1043 | tfa_1197 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1043 | tfa_1221 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1043 | tfa_1222 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1043 | tfa_1223 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1043 | tfa_1252 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1043 | tfa_1258 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1043 | tfa_1268 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1043 | tfa_1274 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1208 | tfa_1226 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1208 | tfa_1227 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1208 | tfa_1228 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1208 | tfa_1229 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1208 | tfa_1230 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1208 | tfa_1231 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1208 | tfa_1232 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1208 | tfa_1253 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1208 | tfa_1259 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1208 | tfa_1269 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1208 | tfa_1275 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1116 | tfa_1189 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1116 | tfa_1233 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1116 | tfa_1260 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1116 | tfa_1276 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1122 | tfa_1198 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1122 | tfa_1234 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1122 | tfa_1264 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1122 | tfa_1277 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1051 | tfa_1200 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1051 | tfa_1235 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1051 | tfa_1278 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1051 | tfa_1279 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1057 | tfa_1202 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1057 | tfa_1236 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1057 | tfa_1265 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1057 | tfa_1280 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1063 | tfa_1281 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1063 | tfa_1203 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1063 | tfa_1237 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1063 | tfa_1266 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1069 | tfa_1282 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1069 | tfa_1204 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1069 | tfa_1238 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1069 | tfa_1267 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1075 | tfa_1155 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1075 | tfa_1192 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1075 | tfa_1193 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1075 | tfa_1206 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1075 | tfa_1249 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1075 | tfa_1250 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1075 | tfa_1251 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1075 | tfa_1255 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1075 | tfa_1261 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1075 | tfa_1270 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1156 | tfa_1168 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1156 | tfa_1194 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1156 | tfa_1195 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1156 | tfa_1207 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1156 | tfa_1246 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1156 | tfa_1247 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1156 | tfa_1248 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1156 | tfa_1256 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1156 | tfa_1262 | https://www.babson.edu/graduate/admissions/thank-you/ |
      | https://www.babson.edu/graduate/ | tfa_1156 | tfa_1271 | https://www.babson.edu/graduate/admissions/thank-you/ |
