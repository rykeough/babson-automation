@Form @152 @Graduate
Feature: admission/graduate-school/
  I want to submit and verify form submits with the correct data

  @Grad @152 @smoke
  Scenario Outline: Submit Certificate Management Form
    Given I am on the homepage
    When I am on the form page
    And I input the first and last name on the form
    And I input an email address
    And I select a program
    And I select a term
    And I input a phone number
    And I select a country
    And I input a state
    And I input a city
    And I input the postal code
    And I click the submit button
    And I am on the correct thankyou page

    Examples: 
      | <form>                                                               | program  | term     | thankYou                                                                      |
      | http://www.babson.edu/academics/graduate-school/mba/full-time-mba/ | tfa_1184 | tfa_1042 | https://www.babson.edu/academics/graduate-school/mba/full-time-mba/thank-you/ |
      | http://www.babson.edu/academics/graduate-school/mba/full-time-mba/ | tfa_1184 | tfa_1089 | https://www.babson.edu/academics/graduate-school/mba/full-time-mba/thank-you/ |
      | http://www.babson.edu/academics/graduate-school/mba/full-time-mba/ | tfa_1184 | tfa_1042 | https://www.babson.edu/academics/graduate-school/mba/full-time-mba/thank-you/ |
      | http://www.babson.edu/academics/graduate-school/mba/full-time-mba/ | tfa_1184 | tfa_1089 | https://www.babson.edu/academics/graduate-school/mba/full-time-mba/thank-you/ |
      | http://www.babson.edu/academics/graduate-school/mba/full-time-mba/ | tfa_1184 | tfa_1186 | https://www.babson.edu/academics/graduate-school/mba/full-time-mba/thank-you/ |
