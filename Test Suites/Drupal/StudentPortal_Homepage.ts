<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>StudentPortal_Homepage</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient>rkeough@babson.edu;tduby@babson.edu;</mailRecipient>
   <numberOfRerun>1</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>true</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>dd414ab9-fa78-478d-9d68-a535184bb9e9</testSuiteGuid>
   <testCaseLink>
      <guid>527ae5a8-5067-4f1c-8a65-955bddf8b5cd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Drupal/StudentPortal_Check</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
