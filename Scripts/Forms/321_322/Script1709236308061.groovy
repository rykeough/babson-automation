import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('https://www.babson.edu')

WebUI.setViewPortSize(1536, 960)

WebUI.navigateToUrl('https://www.babson.edu/grad/emss/part-time-graduate-programs/')

WebUI.setText(findTestObject('FormFields/inputFirstName'), 'Test')

WebUI.setText(findTestObject('FormFields/inputLastName'), 'exrtest')

WebUI.setText(findTestObject('FormFields/inputEmail'), 'autotest@babson-test.com')

WebUI.selectOptionByValue(findTestObject('FormFields/selectPOI'), 'tfa_1037', true)

WebUI.selectOptionByValue(findTestObject('FormFields/selectTOE'), 'tfa_1381', true)

WebUI.setText(findTestObject('FormFields/inputPhoneNumber'), '11011010111')

WebUI.selectOptionByLabel(findTestObject('policy/privacyPolicy'), 'Babados', true)

