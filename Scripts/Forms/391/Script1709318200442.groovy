import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.babson.edu/professional/request-information-bee/')

WebUI.setViewPortSize(1536, 960)

WebUI.click(findTestObject('FormFields/privacyPolicy'))

WebUI.setText(findTestObject('FormFields/inputFirstName'), 'Test')

WebUI.setText(findTestObject('FormFields/inputLastName'), 'erxtest')

WebUI.setText(findTestObject('FormFields/inputInstitution'), 'Test')

WebUI.setText(findTestObject('Object Repository/FormFields/inputTitle'), 'Erxtest')

WebUI.setText(findTestObject('FormFields/inputEmail'), 'test@test-babson.edu')

WebUI.setText(findTestObject('FormFields/inputPhoneNumber'), '1101110011')

WebUI.selectOptionByLabel(findTestObject('FormFields/selectCountry'), 'Babados', true)

WebUI.setText(findTestObject('Object Repository/FormFields/inputHowCanHelp'), 'erxtest Test')

