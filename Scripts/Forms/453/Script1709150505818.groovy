import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.thoughtworks.selenium.webdriven.commands.SelectOption as SelectOption
import com.sun.org.apache.bcel.internal.generic.Select as Select
import com.kms.katalon.core.webui.keyword.builtin.SelectOptionByIndexKeyword as SelectOptionByIndexKeyword
import java.lang.String as String

WebUI.openBrowser('https://www.babson.edu')

WebUI.delay(2)

WebUI.setViewPortSize(1536, 960)

WebUI.delay(3)

WebUI.click(findTestObject('EduHomepage/privacyPolicy'))

WebUI.navigateToUrl(form)

WebUI.delay(3)

WebUI.click(findTestObject('FormFields/VirtualPopUp'))

WebUI.setText(findTestObject('FormFields/inputFirstName'), 'Test')

WebUI.setText(findTestObject('FormFields/inputLastName'), 'erxtest')

WebUI.setText(findTestObject('FormFields/inputEmail'), 'erxtest@babson-test.com')

WebUI.selectOptionByValue(findTestObject('FormFields/selectPOI'), program, false)

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('FormFields/selectTOE'), term, false)

WebUI.setText(findTestObject('FormFields/inputPhoneNumber'), '1011010111')

WebUI.selectOptionByLabel(findTestObject('FormFields/selectCountry'), 'Barbados', false)

WebUI.setText(findTestObject('FormFields/inputState'), 'TestState')

WebUI.setText(findTestObject('FormFields/inputCity'), 'TestCity')

WebUI.setText(findTestObject('FormFields/PostalCode'), '11111')

WebUI.click(findTestObject('FormFields/clickSubmit'))

WebUI.delay(4)

def tyurl = WebUI.getUrl()

WebUI.verifyEqual(tyurl, thankYou)

WebUI.delay(2)

WebUI.deleteAllCookies()

WebUI.delay(2)

WebUI.closeBrowser()

