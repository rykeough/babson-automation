import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://www.babson.edu/')

WebUI.click(findTestObject('Object Repository/MENUEDU/i_Feedback_fas fa-times'))

WebUI.click(findTestObject('Object Repository/MENUEDU/a_Explore Babson'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/MENUEDU/a_Babson At a Glance'))

WebUI.click(findTestObject('Object Repository/MENUEDU/a_A Babson Education'))

WebUI.click(findTestObject('Object Repository/MENUEDU/svg_Babson CollegeBabson College logo'))

WebUI.click(findTestObject('Object Repository/MENUEDU/a_Undergraduate'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/MENUEDU/a_Why Babson'))

WebUI.click(findTestObject('Object Repository/MENUEDU/a_Academics'))

WebUI.click(findTestObject('MENUEDU/svg_Babson CollegeBabson College logo'))

WebUI.click(findTestObject('Object Repository/MENUEDU/a_Graduate'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/MENUEDU/a_Student Experience'))

WebUI.click(findTestObject('Object Repository/MENUEDU/a_News'))

WebUI.click(findTestObject('MENUEDU/svg_Babson CollegeBabson College logo'))

WebUI.click(findTestObject('Object Repository/MENUEDU/a_Professional'))

WebUI.click(findTestObject('MENUEDU/svg_Babson CollegeBabson College logo'))

WebUI.click(findTestObject('Object Repository/MENUEDU/a_Alumni'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/MENUEDU/a_Attend Events'))

WebUI.click(findTestObject('Object Repository/MENUEDU/a_About Us'))

WebUI.click(findTestObject('MENUEDU/svg_Babson CollegeBabson College logo'))

WebUI.closeBrowser()

