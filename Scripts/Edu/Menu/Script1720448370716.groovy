import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.babson.edu/')

WebUI.maximizeWindow()

WebUI.click(findTestObject('Object Repository/EduMenu/i_Feedback_fas fa-times'))

WebUI.click(findTestObject('Object Repository/EduMenu/a_Explore Babson'))

WebUI.delay(2)

WebUI.click(findTestObject('EduMenu/a_Babson At a Glance'))

WebUI.delay(2)

WebUI.click(findTestObject('EduMenu/a_A Babson Education'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/EduMenu/a_Leadership  Faculty'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/EduMenu/svg_Babson CollegeBabson College logo'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/EduMenu/a_Undergraduate'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/EduMenu/a_Admission'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/EduMenu/svg_Babson CollegeBabson College logo (1)'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/EduMenu/a_Graduate'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/EduMenu/a_Student Experience'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/EduMenu/svg_Babson CollegeBabson College logo (2)'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/EduMenu/a_Individuals'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/EduMenu/svg_Babson CollegeBabson College logo (3)'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/EduMenu/a_Alumni'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/EduMenu/a_Access Resources'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/EduMenu/a_Get Involved'))

WebUI.delay(2)

WebUI.click(findTestObject('EduMenu/svg_Babson CollegeBabson College logo'))

WebUI.closeBrowser()

